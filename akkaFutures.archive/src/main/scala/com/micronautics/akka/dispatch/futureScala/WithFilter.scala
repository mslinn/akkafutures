package com.micronautics.akka.dispatch.futureScala

import java.util.concurrent.Executors
import akka.dispatch.{Await,ExecutionContext, Future}
import akka.util.duration.intToDurationInt

object WithFilter extends App {
  val executorService = Executors.newFixedThreadPool(10)
  implicit val context = ExecutionContext.fromExecutor(executorService)

  val future1 = Future(2 + 3)
  Await.result(future1, 1 second)

  val filteredFuture1a = future1 withFilter { _ > 0 }
  println("future1 withFilter { _ > 0 }: ")
  filteredFuture1a.foreach(println(_)) // REPL prints 5 but this prints nothing

  val filteredFuture1b = future1 withFilter { _ > 0 } withFilter{ _ < 6 }
  println("future1 withFilter { _ > 0 } withFilter{ _ < 6 }: ")
  println("filteredFuture1b.length: " + filteredFuture1b)
  filteredFuture1b.foreach(println(_)) // REPL prints 5 but this prints nothing

  val future2 = Future(3 - 12)
  Await.result(future2, 1 second)
  val filteredFuture2 = future2 withFilter { _ > 0 }
  println("future2 withFilter { _ > 0 }: ")
  filteredFuture2.foreach(println(_)) // prints 5 but should print nothing
  
  executorService.shutdown()
}