package com.micronautics.akka.dispatch.futureScala

import java.net.URL
import akka.util.duration.intToDurationInt
import scalax.io.JavaConverters.asInputConverter
import scalax.io.Codec
import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.dispatch.{MessageDispatcher, Await, Future}
import java.util.concurrent.TimeoutException

/** Uses future filters to return a list of Future[String] containing the contents of web pages
  * that contain the string {{{Simpler Concurrency}}}.
  */
object FilterBlocking extends App {
  private val configString: String = "akka { logConfigOnStart=off\n daemonic=on }"
  private val system: ActorSystem = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString))
  implicit private var dispatcher: MessageDispatcher = system.dispatcher

  val urlStrs = List (
    "http://akka.io/",
    "http://www.playframework.org/",
    "http://nbronson.github.com/scala-stm/"
  )

  val contentList = for (urlStr<-urlStrs) yield filterPage(urlStr)
  // Use List.filter to prune out all the pages that had a MatchError (they are stored as empty strings),
  // then print surrounding 80 characters of matching pages
  contentList filter { _.length>0 } foreach { contents =>
    val matchStart = contents.indexOf("Simpler Concurrency")
    println("Scala Filter blocking result: ..." + contents.substring(matchStart-40, matchStart+40) + "...")
  }

  /** @return the contents of the page if it contains the string "Simpler Concurrency",
   * otherwise return the empty string */
  def filterPage(urlStr:String):String = {
     val f1 = Future(httpGet(urlStr))
     val f2 = f1.filter(_.contains("Simpler Concurrency")).recover {
       // value for f2 is the empty string if the evaluated future fails the filter predicate
       case m: MatchError => ""
    }
    try {
      return Await.result(f2, 5 seconds)
    } catch {
      case te:TimeoutException =>
        println("TimeoutException for " + urlStr)
        return ""
    }
  }

  /** Fetches contents of web page pointed to by urlStr */
  def httpGet(urlStr:String):String =
    new URL(urlStr).asInput.slurpString(Codec.UTF8)
}