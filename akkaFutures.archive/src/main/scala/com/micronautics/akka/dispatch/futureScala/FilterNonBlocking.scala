package com.micronautics.akka.dispatch.futureScala

import java.net.URL

import scalax.io.JavaConverters.asInputConverter
import scalax.io.Codec
import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.dispatch.{MessageDispatcher, Future}

/**
  * This example uses future filters to return a list of Future[String] containing the URLs
  * of web pages that contain the string {{{Simpler Concurrency}}}.
 */
object FilterNonBlocking extends App {
  private val configString: String = "akka { logConfigOnStart=off }"
  private val system: ActorSystem = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString))
  implicit private var dispatcher: MessageDispatcher = system.dispatcher

  val urls = List (
    "http://akka.io/",
    "http://www.playframework.org/",
    "http://nbronson.github.com/scala-stm/"
  )

  // print URLs of pages containing the string "Simpler Concurrency"
  urls map ( url =>
    Future {
      httpGet(url)
    } filter {
      _.contains("Simpler Concurrency") // invoked after future completes
    } onComplete { // runs after the filter is evaluated
      case Right(result) => println("Scala Filter non-blocking result: " + url)
      case Left(_:MatchError) => // if the filter does not match, the exception will contain a benign MatchError
      case Left(exception) =>
        val msg = exception.getMessage()
        println(exception.getClass().getName() + " " + msg.substring(msg.lastIndexOf("(")) + " for " + url)
    } andThen {
      case _ => system.shutdown()
    }
  )

  /** Fetches contents of web page pointed to by urlStr */
  def httpGet(urlStr:String):String = {
    new URL(urlStr).asInput.slurpString(Codec.UTF8)
  }
}