package com.micronautics.akka.dispatch.futureJava;

import akka.dispatch.ExecutionContext;
import akka.dispatch.Filter;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.OnComplete;
import com.micronautics.util.HttpGetterWithUrl;
import com.micronautics.util.UrlAndContents;
import scala.MatchError;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/** This example is similar to FilterNonBlocking2 in that extra method definitions to increase legibility, at the expense of increased verbosity.
 * This example associates urls with their page contents by using the HttpGetterWithUrl helper class. */
public class FilterNonBlocking2b {
    private final ExecutorService executorService = Executors.newFixedThreadPool(10);

    private final ExecutionContext context = new ExecutionContext() {
        @Override public void execute(Runnable runnable) { executorService.execute(runnable); }

        public void reportFailure(Throwable throwable) { System.err.println(throwable); }
    };

    private final List<HttpGetterWithUrl> httpGetters = new LinkedList<HttpGetterWithUrl> (Arrays.asList(
        new HttpGetterWithUrl("http://akka.io/"),
        new HttpGetterWithUrl("http://www.playframework.org/"),
        new HttpGetterWithUrl("http://nbronson.github.com/scala-stm/")
    ));

    @SuppressWarnings("unchecked")
    private final OnComplete<UrlAndContents> completionFn = new OnComplete<UrlAndContents>() {
    	/** This method is executed asynchronously, probably after the mainline has completed */
        @Override public void onComplete(Throwable throwable, UrlAndContents result) {
            if (throwable instanceof MatchError)
                return;
            System.out.println("Nonblocking Java filter 2b result: " + result.url);
        }
    };

    /** Invoked after future completes
     * Java type checking does not give clues as to the required types for Function */
    private final Filter<UrlAndContents> filterFn = new Filter<UrlAndContents>() {
    	@Override public boolean filter(UrlAndContents urlAndContents) {
            return urlAndContents.contents.contains("Simpler Concurrency");
        }
    };

    @SuppressWarnings("unchecked")
    private final OnComplete<Iterable<UrlAndContents>> shutdownFn = new OnComplete<Iterable<UrlAndContents>>() {
        @Override public void onComplete(final Throwable throwable, final Iterable<UrlAndContents> result) {
            executorService.shutdown(); // allows program to System.exit()
        }
    };


    public void doit() {
        List<Future<UrlAndContents>> futures = new LinkedList<Future<UrlAndContents>>();
        for (HttpGetterWithUrl httpGetter : httpGetters) {
        	Future<UrlAndContents> resultFuture = Futures.future(httpGetter, context).filter(filterFn);
            resultFuture.onComplete(completionFn);
            futures.add(resultFuture);
        }
        Futures.sequence(futures, context).onComplete(shutdownFn);
    }

    public static void main(String[] args) {
        new FilterNonBlocking2b().doit();
    }
}
