package com.micronautics.akka.dispatch.futureJava;

import akka.actor.ActorSystem;
import akka.dispatch.Filter;
import akka.dispatch.Future;
import akka.dispatch.Future.FutureWithFilter;
import akka.dispatch.Futures;
import akka.dispatch.MessageDispatcher;
import akka.dispatch.OnComplete;
import com.typesafe.config.ConfigFactory;

import java.util.concurrent.Callable;

class WithFilterNonBlocking {
    private final String configString = "akka { logConfigOnStart=off }";
    ActorSystem system = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString));
    MessageDispatcher dispatcher = system.dispatcher();

    @SuppressWarnings("unchecked")
	private final OnComplete<Integer> completionFn = new OnComplete<Integer>() {
        @Override
		public void onComplete(final Throwable throwable, final Integer result) {
            if (result != null) {
                System.out.println("Nonblocking Java WithFilter result: " + result);
            } else {
                System.out.println("Nonblocking Java WithFilter exception: " + throwable);
            }
        }
    };

    private final Filter<Integer> filterFn = new Filter<Integer>() {
        @Override public boolean filter(Integer value) { return value>0; }
    };

    private final Callable<Integer> callable = new Callable<Integer>() {
        @Override public Integer call() { return 2 + 3; }
    };

    @SuppressWarnings("unchecked")
    private final OnComplete<Integer> shutdownFn = new OnComplete<Integer>() {
        @Override public void onComplete(final Throwable throwable, final Integer result) {
            System.out.println("Shutting down");
            system.shutdown(); // allows program to System.exit()
        }
    };


    public void doit() {
        final Future<Integer> resultFuture = Futures.future(callable, dispatcher);
        final FutureWithFilter futureWithFilter = resultFuture.withFilter(filterFn);
        resultFuture.onComplete(completionFn).andThen(shutdownFn);
    }

    public static void main(final String[] args) {
        new WithFilterNonBlocking().doit();
    }
}