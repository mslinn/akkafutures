package com.micronautics.akka.dispatch.futureJava;

import akka.actor.ActorSystem;
import akka.dispatch.Filter;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.MessageDispatcher;
import akka.dispatch.OnComplete;
import akka.dispatch.Recover;
import com.micronautics.util.HttpGetter;
import com.typesafe.config.ConfigFactory;
import scala.MatchError;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * This example introduces extra method definitions to increase legibility, at the expense of increased verbosity.
 * Another difference from the Scala version is that the input (urls) are not associated with the resulting web pages; this
 * is due to a difference in how scopes are managed between Scala and Java. FilterNonBlocking2b corrects this problem.*/
public class FilterNonBlocking2a {
    private final String configString = "akka { logConfigOnStart=off }";
    ActorSystem system = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString));
    MessageDispatcher dispatcher = system.dispatcher();

    private final List<HttpGetter> httpGetters = new LinkedList<HttpGetter> (Arrays.asList(
        new HttpGetter("http://akka.io/"),
        new HttpGetter("http://www.playframework.org/"),
        new HttpGetter("http://nbronson.github.com/scala-stm/")
    ));

    @SuppressWarnings("unchecked")
    private final OnComplete<String> completionFn = new OnComplete<String>() {
    	/** This method is executed asynchronously, probably after the mainline has completed.
    	 * Cannot associate the url with the resulting page contents */
        @Override public void onComplete(final Throwable throwable, final String result) {
            if (throwable instanceof MatchError)
                return;
            if (throwable!=null) {
                System.err.println("Failed: " + throwable.getMessage());
                return;
            }
            int i = result.indexOf("Simpler Concurrency");
            System.out.println("Nonblocking Java filter 2a result: ..." + result.substring(i-20, i+40).trim() + "...");
        }
    };

    /** Invoked after future completes */
    private final Filter<String> filterFn = new Filter<String>() {
    	@Override public boolean filter(final String urlStr) {
            return urlStr.contains("Simpler Concurrency");
        }
    };

    @SuppressWarnings("unchecked")
    private final Recover<Iterable<String>> recoverFn = new Recover<Iterable<String>>() {
        @Override public Iterable<String> recover(final Throwable throwable) {
            System.err.println("Recover: " + throwable.getMessage());
            return new LinkedList<String>();
        }
    };

    @SuppressWarnings("unchecked")
    private final OnComplete<Iterable<String>> shutdownFn = new OnComplete<Iterable<String>>() {
        @Override public void onComplete(final Throwable throwable, final Iterable<String> result) {
            system.shutdown(); // allows program to System.exit()
        }
    };

    public void doit() {
        List<Future<String>> futures = new LinkedList<Future<String>>();
        for (final HttpGetter httpGetter : httpGetters) {
            final Future<String> resultFuture = Futures.future(httpGetter, dispatcher).filter(filterFn);
            resultFuture.onComplete(completionFn);
            futures.add(resultFuture);
        }
        Futures.sequence(futures, dispatcher).recover(recoverFn).andThen(shutdownFn);
    }

    public static void main(final String[] args) {
        new FilterNonBlocking2a().doit();
    }
}
