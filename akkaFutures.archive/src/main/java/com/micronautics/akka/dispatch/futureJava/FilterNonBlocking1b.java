package com.micronautics.akka.dispatch.futureJava;

import akka.dispatch.*;
import akka.dispatch.Future;
import com.micronautics.util.HttpGetterWithUrl;
import com.micronautics.util.UrlAndContents;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

/**  This example minimizes method definitions, like the Scala version, but unlike the Scala version is not very readable.
 * This example associates urls with their page contents by using the HttpGetterWithUrl helper class. */
public class FilterNonBlocking1b {
    /** executorService creates regular threads, which continue running when the application tries to exit. */
    private final ExecutorService executorService = Executors.newFixedThreadPool(10);

    /** Akka uses the execution context to manage futures under its control. This ExecutionContext creates regular threads. */
    private final ExecutionContext context = new ExecutionContext() {
        @Override public void execute(final Runnable runnable) { executorService.execute(runnable); }

        @SuppressWarnings("unused")
		public void reportFailure(Throwable throwable) { System.err.println(throwable); }
    };

    private final List<HttpGetterWithUrl> httpGettersWithUrl = new LinkedList<HttpGetterWithUrl> (Arrays.asList(new HttpGetterWithUrl[] {
    	new HttpGetterWithUrl("http://akka.io/"),
    	new HttpGetterWithUrl("http://www.playframework.org/"),
    	new HttpGetterWithUrl("http://nbronson.github.com/scala-stm/")
    }));


    @SuppressWarnings("unchecked")
    public void doit() {
        for (final HttpGetterWithUrl httpGetterWithUrl : httpGettersWithUrl) {
            final Future<UrlAndContents> resultFuture = Futures.future(httpGetterWithUrl, context);
            final Future<UrlAndContents> filteredFuture = resultFuture.filter(new Filter<UrlAndContents>() {
            	@Override
				public boolean filter(UrlAndContents urlAndContents) {
                    return urlAndContents.contents.contains("Simpler Concurrency");
                }
            });
            filteredFuture.onComplete(new OnComplete<UrlAndContents>() {
                @Override
                public void onComplete(final Throwable exception, final UrlAndContents urlAndContents) {
                    if (exception==null) {
                        System.out.println("Nonblocking Java filter 1b result: " + urlAndContents.url);
                    } else if (exception instanceof scala.MatchError) {
                        // ignore benign MatchRrror
                    } else {
                        System.out.println("Nonblocking Java filter 1b exception: " + exception);
                    }
                }
            });
        }
    }

    public static void main(final String[] args) {
        new FilterNonBlocking1b().doit();
    }
}
