package com.micronautics.akka.dispatch.futureJava;

import akka.dispatch.*;
import com.micronautics.util.HttpGetter;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/** This example minimizes method definitions, like the Scala version, but unlike the Scala version is not very readable.
 * Another difference from the Scala version is that the input (urls) are not associated with the resulting web pages; this
 * is due to a difference in how scopes are managed between Scala and Java. FilterNonBlocking1b and FilterNonBlocking2b correct this problem. */
public class FilterNonBlocking1a {
    /** executorService creates regular threads, which continue running when the application tries to exit. */
    private final ExecutorService executorService = Executors.newFixedThreadPool(10);

    /** Akka uses the execution context to manage futures under its control. This ExecutionContext creates regular threads. */
    private final ExecutionContext context = new ExecutionContext() {
        @Override public void execute(final Runnable runnable) { executorService.execute(runnable); }

        @SuppressWarnings("unused")
		public void reportFailure(Throwable throwable) { System.err.println(throwable); }
    };

    private final List<HttpGetter> httpGetters = new LinkedList<HttpGetter> (Arrays.asList(new HttpGetter[] {
    	new HttpGetter("http://akka.io/"),
    	new HttpGetter("http://www.playframework.org/"),
    	new HttpGetter("http://nbronson.github.com/scala-stm/")
    }));


    @SuppressWarnings("unchecked")
    public void doit() {
        for (final HttpGetter httpGetter : httpGetters) {
            final Future<String> resultFuture = Futures.future(httpGetter, context);
            resultFuture.filter(new Filter<String>() {
            	@Override
				public boolean filter(final String urlStr) {
                    return urlStr.indexOf("Simpler Concurrency")>=0;
                }
            }); // urlStr is out of scope, so it cannot be associated with result in the next block
            resultFuture.onComplete(new OnComplete<String>() {
            	/** This method is executed asynchronously, probably after the mainline has completed */
                @Override
				public void onComplete(final Throwable exception, final String result) {
                    if (result != null) {
                        System.out.println("Nonblocking Java filter 1a result: " + result.substring(0, 20) + "...");
                    } else {
                        System.out.println("Nonblocking Java filter 1a exception: " + exception);
                    }
                    executorService.shutdown(); // Used to shut down idle threads, but now throws Exception
                }
            });
        }
    }

    public static void main(final String[] args) {
        final FilterNonBlocking1a example = new FilterNonBlocking1a();
        example.doit();
    }
}
