Composable Futures with Akka 2.0
================================
Please visit the [companion microsite](https://bitbucket.org/mslinn/akkafutures) for the [book](http://slinnbooks.com/books/futures/index.jsp).
Here you will find source code, instructions for building with SBT, Eclipse and IntelliJ IDEA. You can also log bugs.
Check out the tabs labeled [Wiki](https://bitbucket.org/mslinn/akkafutures/wiki/Home) and [Issues](https://bitbucket.org/mslinn/akkafutures/issues?status=new&status=open).

Although you can download compressed source code from this site, I suggest you clone the git repository instead 
so you can keep up-to-date more easily. Typesafe is moving fast, and the source code and/or build configuration 
will need to be revised frequently. Instructions are in this project's wiki (see the menu above).

I intend to use Google Hangout every week to chat about the book.
This is not meant as free consulting - instead, if there is something that is not clearly expressed in the book, 
or a problem with a code example, I'd love to talk with you about it.

BTW, my Akka 2.0 notes and other Typesafe notes are [here](http://scala.micronauticsresearch.com/akka-2-0).

Mike Slinn

Google+: [mslinn](https://plus.google.com/115543354052259422614/posts)  
Twitter: [mslinn](https://twitter.com/#!/mslinn)

[mslinn@micronauticsresearch.com](mailto:mslinn@micronauticsresearch.com)

415-367-3789 

840 Main St #B2  
Half Moon Bay, CA 94019
