organization := "Micronautics Research"

name := "AkkaFutures"

version := "0.2"

scalaVersion := "2.9.2" // v2.9.2 also required for Akka 2.1-SNAPSHOT

scalaVersion in update := "2.9.2"

autoCompilerPlugins := true

addCompilerPlugin("org.scala-lang.plugins" % "continuations" % "2.9.2")

scalacOptions ++= Seq("-deprecation", "-P:continuations:enable")

scalacOptions in (Compile, console) += "-Yrepl-sync"

scalacOptions in (Compile, doc) <++= baseDirectory.map {
  (bd: File) => Seq[String](
     "-sourcepath", bd.getAbsolutePath,
     "-doc-source-url", "https://bitbucket.org/mslinn/akkafutures/src/9fa9548ce587/akkaFutures€{FILE_PATH}.scala"
  )
}

resolvers ++= Seq(
  "Typesafe Releases"     at "http://repo.typesafe.com/typesafe/releases"
)

libraryDependencies <+= scalaVersion { v => compilerPlugin("org.scala-lang.plugins" % "continuations" % v) }

libraryDependencies ++= Seq(
  "org.scalatest"                 %  "scalatest_2.9.1"     % "1.8"     % "test" withSources(),
  "com.typesafe.akka"             %  "akka-actor"          % "2.0.3"   withSources(),
  "com.typesafe.akka"             %  "akka-agent"          % "2.0.3"   withSources(),
  "com.github.scala-incubator.io" %  "scala-io-core_2.9.1" % "0.4.1"   withSources(),
  "com.github.scala-incubator.io" %  "scala-io-file_2.9.1" % "0.4.1"   withSources(),
  "org.apache.httpcomponents"     %  "httpclient"          % "4.1.2"   withSources(),
  "org.scala-tools"               %  "scala-stm_2.9.1"     % "0.6"     withSources()
)

logLevel := Level.Error

// Optional settings from https://github.com/harrah/xsbt/wiki/Quick-Configuration-Examples follow

// define the statements initially evaluated when entering 'console', 'console-quick', or 'console-project'
initialCommands := """
  import akka.actor._
  import akka.agent._
  import akka.dispatch._
  import akka.util.Duration
  import akka.util.duration._
  import com.micronautics.akka.dispatch.futureScala._
  import com.micronautics.concurrent._
  import com.micronautics.util._
  import java.net.URL
  import java.util.concurrent.Executors
  import java.util.Date
  import scala.MatchError
  import scala.util.Random
  import scalax.io.JavaConverters.asInputConverter
  import scalax.io.Codec
  //
  val random = new Random()
  //
  val executorService = Executors.newCachedThreadPool()
  implicit val context = ExecutionContext.fromExecutor(executorService)
  //
  val daemonExecutorService = DaemonExecutors.newFixedThreadPool(10)
  //implicit val daemonContext = ExecutionContext.fromExecutor(daemonExecutorService)
  //
  def expensiveCalc(x:Int) = { x * x }
  //
  def expCalc(a:Int, b:Int) = {
    Thread.sleep(random.nextInt(200))
    a + b
  }
  //
  def httpGet(urlStr:String):String =
    new URL(urlStr).asInput.string(Codec.UTF8)
  //
  //val f = Future { httpGet("http://akka.io") }
  //
  val urlStrs = List (
    "http://akka.io",
    "http://www.playframework.org",
    "http://nbronson.github.com/scala-stm"
  )
"""

// Only show warnings and errors on the screen for compilations.
// This applies to both test:compile and compile and is Info by default
logLevel in compile := Level.Warn
