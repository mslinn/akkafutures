package com.micronautics.akka.dispatch.futureScala

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.dispatch.{MessageDispatcher, Future}

object Race extends App {
  private val configString: String = "akka { logConfigOnStart=off }"
  private val system: ActorSystem = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString))
  implicit private var dispatcher: MessageDispatcher = system.dispatcher

  @volatile var offset = 6 // @volatile makes no difference because it does not solve race conditions
  def accessor = offset

  val f1 = Future {
    2 + 3 + offset // will be executed asynchronously
  } onComplete { f =>
    system.log.info("Race entered onComplete, offset = " + offset)
    f match {
      case Right(result)   => println("Race Scala result: " + result)
      case Left(exception) => println("Race Scala exception: " + exception.getMessage)
    }
  }

  val f2 = Future { 2 + 3 + accessor } onComplete { f =>
    system.log.info("Entered onComplete, offset = " + offset)
    f match {
      case Right(result)   => println("Race Scala result: " + result)
      case Left(exception) => println("Race Scala exception: " + exception.getMessage)
    }
  }
  offset = 42
  system.log.info("End of mainline, offset = " + offset)

  Future.sequence(List(f1, f2)) andThen {
    case _ => system.shutdown() // terminates this thread, and the program if no other threads are active
  }
}