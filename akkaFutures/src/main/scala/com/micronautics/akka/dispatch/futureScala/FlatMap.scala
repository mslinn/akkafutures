package com.micronautics.akka.dispatch.futureScala

import akka.actor.ActorSystem
import java.util.Date
import akka.dispatch.{Promise, MessageDispatcher, Future}
import java.util.concurrent.CountDownLatch

case class User(val firstName: String, val lastName: String, val signedUp: Date)

/** if auth() returns an Exception, the flatMap in lookupUser() will skip that future */
object FlatMap extends App {
  private val system: ActorSystem = ActorSystem()
  implicit private var dispatcher: MessageDispatcher = system.dispatcher
  private val countDown = new CountDownLatch(2) // 2 users to authenticate in this demo

  loginTest("asdf")
  loginTest("nope")


  /** @return Future containing user id or an Exception. */
  def authenticate(token: String): Future[Long] =
    if (token=="asdf")
      Future(123456)
    else
      Promise.failed(new Exception("Invalid token " + token))

  def getUser(id: Long): Future[User] =
    if (id==123456)
      Future(User("Fred", "Flintstone", new Date(475488000000L)))
    else
      Promise.failed(new Exception("No user with id " + id))

  /** @return Successful future containing User, otherwise short circuit and
    *         return failed Future if auth() fails */
  def lookupUser(token: String): Future[User] =
    authenticate(token) flatMap { id =>
      system.log.info("lookupUser(%s) authenticated with id=%d".format(token, id))
      getUser(id)
    }

  def loginTest(token: String) {
    val user: Future[User] = lookupUser(token)
    user onComplete { f =>
      f match {
        case Left(exception) => system.log.info(exception.getMessage)
        case Right(value: User) => system.log.info(value.toString)
      }
      countDown.countDown()
      if (countDown.getCount==0)
        system.shutdown()
    }
  }
}