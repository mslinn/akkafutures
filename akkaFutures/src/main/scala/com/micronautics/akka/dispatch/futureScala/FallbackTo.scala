package com.micronautics.akka.dispatch.futureScala


import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.dispatch.{MessageDispatcher, Future}


object FallbackTo extends App {
  private val configString: String = "akka { logConfigOnStart=off }"
  private val system: ActorSystem = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString))
  implicit private var dispatcher: MessageDispatcher = system.dispatcher

  val f1 = Future { system.log.info("Evaluating f1"); 6 / 0 }
  val f2 = Future { system.log.info("Evaluating f2"); "asdf" + "qwer" }
  val f3 = Future { system.log.info("Evaluating f3"); 2 + 3 }

  (f1 fallbackTo f2) andThen {
    case Right(result)   => system.log.info("Nonblocking FallbackTo Scala result: " + result)
    case Left(exception) => system.log.info("Nonblocking FallbackTo Scala exception: " + exception.getMessage())
  } andThen {
    case _ =>
      system.shutdown(); // allow System.exit()
  }
}