package com.micronautics.akka.dispatch

import akka.dispatch.Future
import com.typesafe.config.ConfigFactory
import akka.actor.ActorSystem

/** Configure Akka system from a string */
object ConfigScala2 extends App {
  val configString = """
    akka {
      log-config-on-start=on      # dumps out configuration onto console when enabled and loglevel >= "INFO"
      stdout-loglevel = "WARNING" # startup log level
      loglevel = "INFO"           # loglevel once ActorSystem is started
      actor {
        default-dispatcher {
          type = Dispatcher
          max-pool-size-max = 64  # default
          throughput = 5          # default
          core-pool-size-max = 64 # default
        }
      }
    }
    """
  val system = ActorSystem("default", ConfigFactory.parseString(configString))
  implicit val dispatcher = system.dispatcher

  Future {
    2 + 3
  } andThen {
    case Right(result) => system.log.info("ConfigScala2 result: " + result)
    case Left(exception) => system.log.info("ConfigScala2 exception: " + exception)
  } andThen {
    case _ =>
      system.log.error("This an an error log message");
      system.shutdown() // allow System.exit()
  }
}