package com.micronautics.akka.dispatch.futureScala

import akka.dispatch.{Await, ExecutionContext, Future}
import akka.util.duration._
import java.util.concurrent.{TimeoutException, Executors}


object Ready extends App {
  val executorService = Executors.newFixedThreadPool(10)
  implicit val context = ExecutionContext.fromExecutor(executorService)

  val f1 = Future { 2 + 3 }
  try {
    val f2 = Await.ready(f1, 5 millis)
    f2.value.get match {
      case Right(result)   => println("Ready Scala result 1: " + result)
      case Left(exception) => println("Ready Scala exception 1: " + exception)
    }
  } catch {
    case te:TimeoutException => println("Ready Scala TimeoutException 1")
  }

  val f3 = Future {
    Thread.sleep(1000)
    2 + 3
  }
  try {
    val f4 = Await.ready(f3, 5 millis)
    f4.value.get match {
      case Right(result)   => println("Ready Scala result 2: " + result)
      case Left(exception) => println("Ready Scala exception 2: " + exception)
    }
  } catch {
    case te:TimeoutException => println("Ready Scala TimeoutException 2")
  }

  executorService.shutdown(); // allow System.exit()
}