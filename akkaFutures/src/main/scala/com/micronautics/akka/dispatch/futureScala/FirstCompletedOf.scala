package com.micronautics.akka.dispatch.futureScala

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.dispatch.{MessageDispatcher, Future}


object FirstCompletedOf extends App {
  private val configString: String = "akka { logConfigOnStart=off }"
  private val system: ActorSystem = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString))
  implicit private var dispatcher: MessageDispatcher = system.dispatcher

  val fs = List(Future{
      Thread.sleep(100);
      "asdf"
    }, Future{
      Thread.sleep(200);
     1234})

  val fos = Future.firstCompletedOf(fs) andThen {
    case Right(result) => system.log.info("FirstCompletedOf Scala result: " + result)
    case Left(exception) => system.log.info("FirstCompletedOf Scala exception: " + exception.toString)
  } andThen {
    case _ =>
      system.shutdown(); // allow System.exit()
  }
}