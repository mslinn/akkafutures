package com.micronautics.akka.dispatch.futureScala

import akka.actor.ActorSystem
import akka.dispatch._
import com.typesafe.config.ConfigFactory

object Sequence2 extends App {
  val str = "akka { logConfigOnStart=off }"
  implicit val system = ActorSystem("actorSystem", ConfigFactory.parseString(str))

  val futures = List.range(1, 10) map { x => Future(x * 2) }
  val future:Future[Seq[Int]] = Future.sequence{ futures }
  future.onComplete { x => x match {
      case Right(result) =>
        result foreach {i:Int => print(" " + i) }
      case Left(throwable) =>
        print(throwable.getMessage)
    }
    println
    system.shutdown()
  }
}