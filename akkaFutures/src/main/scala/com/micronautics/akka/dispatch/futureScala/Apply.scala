package com.micronautics.akka.dispatch.futureScala

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.dispatch.{MessageDispatcher, Future}


object Apply extends App {
  private val configString: String = "akka { logConfigOnStart=off }"
  private val system: ActorSystem = ActorSystem("actorSystem", ConfigFactory.parseString(configString))
  implicit private val dispatcher: MessageDispatcher = system.dispatcher

  Future { 2 + 3 } andThen {
    case Right(result)   => system.log.info("Apply Scala result: " + result)
    case Left(exception) => system.log.info("Apply Scala exception: " + exception.toString)
  } andThen {
    case _ =>
      system.shutdown(); // allow System.exit()
  }
}