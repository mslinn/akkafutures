package com.micronautics.akka.dispatch.futureScala

import java.util.concurrent.Executors
import akka.dispatch.{ExecutionContext, Future}


object Sequence extends App {
  val executorService = Executors.newFixedThreadPool(20) // change the number of threads and view output
  implicit val context = ExecutionContext.fromExecutor(executorService)
  implicit val countUp: Boolean = false // make false to change Future completion order

  if (countUp)
    println("Last Future started will complete last")
  else
    println("Last Future started will complete first")

  val stringFutures = for (i <- 1 to 10) yield Future {
    val tTime = futureTime(i)
    println("Original future " + i + " on " + Thread.currentThread().getName + " takes " + tTime + " ms.")
    Thread.sleep(tTime)
    i.toString
  }

  Future sequence stringFutures andThen { case f =>
    f match {
      case Right(result)   =>
        println("Sequence Scala result: " + result)
      case Left(exception) =>
        println("Sequence Scala exception: " + exception.getMessage)
    }
    println("The thread that Future.sequence runs on depends on many factors: " + Thread.currentThread().getName)
  } andThen {
    case _ =>
        executorService.shutdown() // allow System.exit() to execute
  }

  private def futureTime(i: Int)(implicit countUp: Boolean): Long =
    if (countUp)
      50 * i
    else
      600 - 50 * i
}