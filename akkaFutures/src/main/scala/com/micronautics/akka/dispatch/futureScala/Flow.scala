package com.micronautics.akka.dispatch.futureScala

import akka.actor.ActorSystem
import akka.dispatch._
import akka.dispatch.Future.flow
import akka.util.Duration

/** flow() needs the Delimited Continuations compiler plugin.
 * The Akka docs ask you to do things the hard way. Just add the following to build.sbt:
 * <pre>autoCompilerPlugins := true
 *
 * addCompilerPlugin("org.scala-lang.plugins" % "continuations" % "2.9.1")
 *
 * scalacOptions += "-P:continuations:enable"</pre>
 * Be sure to use the same version of the continuations plugin as your Scala version.
 *
 * @see https://github.com/harrah/xsbt/wiki/Compiler-Plugins
 * @see http://akka.io/api/akka/snapshot/#akka.dispatch.Future$
 * @see http://akka.io/docs/akka/snapshot/scala/dataflow.html
 * @author Mike Slinn*/

object Flow extends App {
  val system = ActorSystem()
  implicit val dispatcher = system.dispatcher

  val f1 = Future {
    Thread.sleep(200)
    "To be or not to be"
  }

  val f2 = Promise[String]()

  val f3 = flow {
    println("f2 and f3 are being set to value of " + f1)
    f2 << f1()
  } onComplete {
    case Right(result) => system.log.info("f3 value is " + result)
    case Left(throwable) => system.log.info("f3 threw " + throwable.getMessage)
  }
  f2 onComplete {
    case Right(result) => system.log.info("f2 value is " + result)
    case Left(throwable) => system.log.info("f2 threw " + throwable.getMessage)
  }
  println("Waiting for f3 to complete")
  Await.ready(f3, Duration.Inf)
  system.shutdown()
}