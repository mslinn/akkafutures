package com.micronautics.akka.dispatch.futureScala

import akka.dispatch._
import akka.util.Duration
import scala.util.Random
import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory

/** @author Derek Williams
 * @author Mike Slinn */
object MapFilter extends App {
  private val configString: String = "akka { logConfigOnStart=off }"
  private val system: ActorSystem = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString))
  implicit private var dispatcher: MessageDispatcher = system.dispatcher

  val random = new Random()

  val futures: List[Future[Int]] = List(Future(expCalc(1, 2)), Future(expCalc(11, 2)), Future(expCalc(3, 5)))

  val f: Future[List[Option[Int]]] = Future.traverse(futures) {
    _ map { // This is List.map
      case x if x<10 => Some(x) // List.filter() is implicitly invoked
      case _ => None
    }
  }
  // walk through list and flatten each item (turn Option[Int]s into Ints)
  // List.map discards items that are None
  val g: Future[List[Int]] = f map { _.flatten } // Invokes Future.map and List.flatten

  val result: List[Int] = Await.result(g, Duration.Inf) // block until result is ready; should be almost instant
  system.log.info("MapFilter result = " + result) // prints List(3, 8)
  system.shutdown

  /** Pretend that this is an expensive computation that can take up to 200 ms */
  def expCalc(a: Int, b: Int) = {
    Thread.sleep(random.nextInt(200))
    a + b
  }
}