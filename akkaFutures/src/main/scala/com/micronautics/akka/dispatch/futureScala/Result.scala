package com.micronautics.akka.dispatch.futureScala

import akka.dispatch.{Await, ExecutionContext, Future}
import akka.util.duration._
import java.util.concurrent.{TimeoutException, Executors}


object Result extends App {
  val executorService = Executors.newFixedThreadPool(10)
  implicit val context = ExecutionContext.fromExecutor(executorService)

  val f1 = Future {
    2 + 3
  }
  try {
    println("Scala result 1: " + Await.result(f1, 5 millis))
  } catch {
    case te: TimeoutException => println("Result Scala TimeoutException 1")
  }

  val f2 = Future {
    Thread.sleep(1000)
    2 + 3
  }
  try {
    println("Scala result 2: " + Await.result(f2, 5 millis))
  } catch {
    case te: TimeoutException => println("Result Scala TimeoutException 2")
  }

  executorService.shutdown(); // allow System.exit()
}