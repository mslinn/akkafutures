package com.micronautics.akka.dispatch.futureScala

import akka.util.duration._
import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.dispatch.{MessageDispatcher, Future, Await}

object ReduceBlockingScala extends App {
  private val configString: String = "akka { logConfigOnStart=off\n daemonic=on }"
  private val system: ActorSystem = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString))
  implicit private var dispatcher: MessageDispatcher = system.dispatcher

  val expensiveFutures = (1 to 100) map (x => Future { expensiveCalc(x) })
  val resultFuture = Future.reduce(expensiveFutures)(_ + _)
  val result = Await.result(resultFuture, 1 second)
  system.log.info("Blocking Scala reduce result: " + result)

  /** Pretend this method consumes a lot of computational resource */
  def expensiveCalc(x:Int) = { x * x }
}