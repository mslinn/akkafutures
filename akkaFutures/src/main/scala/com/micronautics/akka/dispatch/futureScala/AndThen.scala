package com.micronautics.akka.dispatch.futureScala

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.dispatch.{MessageDispatcher, Future}

object AndThen extends App {
  private val configString: String = "akka { logConfigOnStart=off }"
  private val system: ActorSystem = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString))
  implicit private var dispatcher: MessageDispatcher = system.dispatcher

  Future(2 + 3) andThen {
      case Right(result) => system.log.info("Scala AndThen result: " + result)
      case Left(exception) => system.log.info(exception.toString)
  } andThen {
      case Right(result) => system.log.info("Almost all done")
      case Left(exception) => system.log.info("Something happened")
  } andThen { case _ =>
    system.log.info("Shutting down")
    system.shutdown() // terminates this thread, and the program if no other threads are active
  }
}