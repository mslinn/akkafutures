package com.micronautics.akka.dispatch.futureScala

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.dispatch.{MessageDispatcher, Future}


object Recover extends App {
  private val configString: String = "akka { logConfigOnStart=off }"
  private val system: ActorSystem = ActorSystem.apply("default", ConfigFactory.parseString(configString))
  implicit private var dispatcher: MessageDispatcher = system.dispatcher

  Future {
    system.log.info("Original future thread " + Thread.currentThread().getName)
    6 / 0
  } onSuccess {
    case result =>
      system.log.info("Future.onSuccess thread " + Thread.currentThread().getName)
      system.log.info("onSuccess 1; result=" + result);
  } onFailure {
    case throwable =>
      system.log.info("Future.onFailure thread " + Thread.currentThread().getName)
      system.log.info("onFailure 1: " + throwable.getMessage());
  } recover {
    case e: ArithmeticException =>
      system.log.info("Future.recover thread " + Thread.currentThread().getName)
      system.log.info("Recover returning largest Integer")
      Int.MaxValue
    case _ =>
      println("Recover returning zero")
      0
  } onSuccess {
    case result =>
      system.log.info("Future.recover.onSuccess thread " + Thread.currentThread().getName)
      system.log.info("onSuccess 2; result=" + result);
  } onFailure {
    case throwable =>
      system.log.info("Future.recover.onFailure thread " + Thread.currentThread().getName)
      system.log.info("onFailure 2: " + throwable.getMessage());
  } andThen {
    case _ =>
      system.log.info("Future.andThen thread " + Thread.currentThread().getName)
      system.shutdown(); // allow System.exit()
  }
}