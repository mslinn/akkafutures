package com.micronautics.akka.dispatch.futureScala

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.dispatch.{MessageDispatcher, Future}


object ReduceNonblockingScala extends App {
  private val configString: String = "akka { logConfigOnStart=off }"
  private val system: ActorSystem = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString))
  implicit private var dispatcher: MessageDispatcher = system.dispatcher

  val expensiveFutures = (1 to 100) map (x => Future { expensiveCalc(x) })

  Future.reduce(expensiveFutures)(_ + _) andThen {
    case Right(result)   => system.log.info("Nonblocking Scala reduce result: " + result)
    case Left(exception) => system.log.info("Nonblocking Scala reduce exception: " + exception.getMessage)
  } andThen { // futures have completed, release resources
    case _ => system.shutdown() // allow System.exit() to run
  }

  /** Pretend this method consumes a lot of computational resource */
  def expensiveCalc(x:Int) = { x * x }
}