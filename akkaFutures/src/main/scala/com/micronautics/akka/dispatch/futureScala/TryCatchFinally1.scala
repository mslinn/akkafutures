package com.micronautics.akka.dispatch.futureScala

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.dispatch.{Await, MessageDispatcher, Future}
import akka.util.Duration

object TryCatchFinally1 extends App {
  private val configString: String = "akka { logConfigOnStart=off }"
  private val system: ActorSystem = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString))
  implicit private var dispatcher: MessageDispatcher = system.dispatcher

def isEven(f:Future[Int]):Future[Boolean] = {
  system.log.info(f.toString)
  // Note that the onSuccess does not change the result of the future passed to andThen:
  f onSuccess { case i => i / 2 } andThen { case x => println(x) }
  f map { // try clause computes future value or throws an exception
    case i => i/(i-1) // evaluated value is then wrapped into a new Future
  } recover { // catch
    case ae:ArithmeticException =>
      system.log.info(ae.getMessage)
      Int.MaxValue // gets wrapped into new Future
    case ex:Exception =>
      system.log.info(ex.getMessage)
      0 // gets wrapped into new Future
  } map { // finally clause returns Future[Boolean] not Future[Int] so map is required
    case v:Int => (v % 2) == 0
    case _ => false
  }
}

for (i <- 3 to 1 by -1)
  try { // If a Future contains an Exception, calling Await.result() will cause the Exception to be rethrown.
    system.log.info(i + ": " + Await.result(isEven(Future(i)), Duration.Inf))
  } catch {
    case exception => system.log.info(exception.getMessage)
  }
  system.shutdown() // terminates this thread, and the program if no other threads are active
}