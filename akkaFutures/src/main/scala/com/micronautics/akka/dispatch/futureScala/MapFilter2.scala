package com.micronautics.akka.dispatch.futureScala

import akka.dispatch._
import java.util.concurrent.Executors


/** Clumsy version of MapFilter. Works similarly, but is not as elegant.
  * Uncomment the println() statements to discover how this works */
object MapFilter2 extends App {
  val executorService = Executors.newFixedThreadPool(10)
  implicit val context = ExecutionContext.fromExecutor(executorService)

  Future.sequence(List(
    refine(Future(1 + 2)) andThen {
      case Right(result) => /*system.log.info("First refined result="+result);*/ result
      case _ =>
    }, refine(Future(11 + 2)) andThen {
      case Right(result) => /*system.log.info("Second refined result="+result);*/ result
      case _ =>
    }, refine(Future(3 + 5)) andThen {
      case Right(result) => result
      case _ =>
    })) onComplete {
    case Right(result) =>
      println("MapFilter2 final result=" + result.filter { _ != () })
      executorService.shutdown()
    case _ =>
  }

  /** Do something similar to Future.filter() but use Future.map()
    * so we do not have to contend with MatchErrors */
  def refine (f: => Future[Int]) = f map { g =>
    //system.log.info("Value to refine=" + g)
    g match {
      case x if x<10 => /*system.log.info("Refined value=" + x);*/ x
      case _ => /*system.log.info("Refine failed predicate");*/ ()
    }
  }
}