package com.micronautics.akka.dispatch.futureScala

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.dispatch.{MessageDispatcher, Future}


object Foreach extends App {
  private val configString: String = "akka { logConfigOnStart=off }"
  private val system: ActorSystem = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString))
  implicit private var dispatcher: MessageDispatcher = system.dispatcher

  val f = Future {
    2 + 3
  }
  f foreach { result =>
    system.log.info("Scala foreach result: " + result)
  }
  f andThen {
    case _ =>
      system.shutdown(); // allow System.exit()
  }
}