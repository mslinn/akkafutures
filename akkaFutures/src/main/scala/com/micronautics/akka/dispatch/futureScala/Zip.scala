package com.micronautics.akka.dispatch.futureScala


import scala.util.Random
import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.dispatch.{MessageDispatcher, Future}

/** If you run this program many times, the order in which the zips complete will be random */
object Zip extends App {
  private val configString: String = "akka { logConfigOnStart=off }"
  private val system: ActorSystem = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString))
  implicit private var dispatcher: MessageDispatcher = system.dispatcher

  val random = new Random()
  val f1 = Future {
    Thread.sleep(random.nextInt(100))
    6 / 0
  }
  val f2 = Future {
    Thread.sleep(random.nextInt(100))
    "asdf" + "qwer"
  }
  val f3 = Future {
    Thread.sleep(random.nextInt(100))
    2 + 3
  }

  val f4 = (f1 zip f2) recover {
    case ex:Exception => (0, 0)
  } onComplete { f =>
    Thread.sleep(random.nextInt(100));
    f match {
      case Right(result)   => system.log.info("Nonblocking Zip Scala result 1: " + result)
      case Left(exception) => system.log.info("Nonblocking Zip Scala exception 1: " + exception)
    }
  }

  (f2 zip f3) recover {
    case ex:ArithmeticException ⇒ (0, 0) // thrown when dividing by zero
    case ex:Exception ⇒ (-1, -1)
  } andThen {
    case Right(result)   => system.log.info("Nonblocking Zip Scala result 2: " + result)
    case Left(exception) => system.log.info("Nonblocking Zip Scala exception 2: " + exception.getMessage)
  } andThen { // futures have completed, release resources
    case _ =>
      f4.andThen { case _ => system.shutdown() } // Allows System.exit() to run
  }
}