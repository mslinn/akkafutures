package com.micronautics.akka.dispatch.futureScala

import java.net.URL

import scalax.io.JavaConverters.asInputConverter
import scalax.io.Codec
import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.dispatch.{MessageDispatcher, Future}


object Traverse extends App {
  private val configString: String = "akka { logConfigOnStart=off }"
  private val system: ActorSystem = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString))
  implicit private var dispatcher: MessageDispatcher = system.dispatcher

  val urls = List (
    "http://akka.io/",
    "http://www.playframework.org/",
    "http://nbronson.github.com/scala-stm/"
  )

  Future.traverse(urls) { // build a Future[List[Int]]
    url => Future { httpGet(url).length() }
  } onComplete { f => // obtain a List[Int]
    f match {
      case Right(result)   => system.log.info("Traverse Scala web page lengths: " + result)
      case Left(exception) => system.log.info("Traverse Scala exception: " + exception)
    }
    system.shutdown()
  }

  /** Fetches contents of web page pointed to by urlStr */
  def httpGet(urlStr:String) = {
    new URL(urlStr).asInput.string(Codec.UTF8)
  }
}