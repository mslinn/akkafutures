package com.micronautics.akka.dispatch

import com.typesafe.config.ConfigFactory
import akka.actor.ActorSystem
import akka.dispatch.{Await, Future}
import akka.util.Duration

/**Configure Akka system with and Executor that spins daemon threads */
object ConfigScalaDaemon extends App {
  val str = "akka { daemonic = on \n logConfigOnStart=off }"
  implicit val system = ActorSystem("actorSystem", ConfigFactory.parseString(str))

  val f = Future(2 + 3)
  val value = Await.result(f, Duration.Inf)
  system.log.info("ConfigScalaDaemon result: " + value)
}