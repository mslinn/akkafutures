package com.micronautics.akka.dispatch

import akka.dispatch.Future
import akka.actor.ActorSystem
import akka.event.Logging

/** Configure Akka system from src/main/resources/application.conf */
object ConfigScala extends App {
  val system = ActorSystem()
  implicit val dispatcher = system.dispatcher

  def time[R](block: => R, msg: String="Elapsed time"): R = {
      val t0 = System.nanoTime()
      val result = block    // call-by-name
      val t1 = System.nanoTime()
    system.log.info(msg + ": "+ (t1 - t0)/1000000 + "ms")
      result
  }
  
  Future { 2 + 3 } andThen {
    case Right(result)   => system.log.info("ConfigScala result: " + result)
    case Left(exception) => system.log.info("ConfigScala exception: " + exception)
  } andThen {
    case _ =>
      system.logConfiguration()
      system.log.info("ActorSystem name=" + system.name)
      system.log.info("Before shutdown: actorSystem isTerminated=" + system.isTerminated)
      system.shutdown() // allow System.exit()
      system.log.info("After shutdown: actorSystem isTerminated=" + system.isTerminated)
      time(system.awaitTermination(), "Time spent awaiting termination")
      system.log.info("After awaitTermination(): actorSystem isTerminated=" + system.isTerminated)
  }
}