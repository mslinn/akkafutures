package com.micronautics.akka.dispatch.futureScala

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.dispatch.{MessageDispatcher, Future}

object Fold2 extends App {
  private val configString: String = "akka { logConfigOnStart=off }"
  private val system: ActorSystem = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString))
  implicit private var dispatcher: MessageDispatcher = system.dispatcher

  val futures = (1 to 10) map (x => Future {
    expensiveCalc(x)
  })

  Future.fold(futures)(0)(_ + _) andThen {
    case Right(result) => system.log.info("Fold2 Scala result: " + result)
    case Left(exception) => system.log.info("Fold2 Scala exception: " + exception)
  } andThen {
    case _ =>
      system.shutdown(); // terminates program
  }

  /**Pretend this method consumes a lot of computational resource */
  def expensiveCalc(x: Int) = {
    x * x
  }
}