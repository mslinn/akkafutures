package com.micronautics.akka.dispatch.futureScala

import java.net.URL

import scalax.io.JavaConverters.asInputConverter
import scalax.io.Codec
import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.dispatch.{MessageDispatcher, Future}

/** Uses Future.map to print URLs of web pages that contain the string {{{Scala}}}. */
object Map extends App {
  private val configString: String = "akka { logConfigOnStart=off }"
  private val system: ActorSystem = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString))
  implicit private var dispatcher: MessageDispatcher = system.dispatcher

  val urls = List (
    "http://akka.io/",
    "http://www.playframework.org/",
    "http://nbronson.github.com/scala-stm/"
  )

  val futures: List[Future[String]] = for (url <- urls) yield Future { httpGet(url) }
  var i: Int = 0
  futures map { future =>
    future map { // implicit onComplete handler; Future value is mapped
      case pageContents if pageContents.contains("Scala") =>
        val i = pageContents.indexOf("Scala")
        system.log.info("Blocking Scala map result: " + pageContents.substring(i-20, i+40))
      case _ => // ignore pages that do not contain the string
    } recover {
      case ex =>
        system.log.info(ex.getClass.getName)
    } andThen { // count down as each future completes, then shut down
      case _ =>
        i += 1
        if (i==futures.length)
          system.shutdown()
    }
  }

  /** Fetches contents of web page pointed to by urlStr */
  def httpGet(urlStr:String):String =
    new URL(urlStr).asInput.string(Codec.UTF8)
}