package com.micronautics.akka.dispatch.futureScala

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import com.micronautics.akka.ExpensiveCalc._
import akka.jsr166y.ForkJoinPool
import akka.util.Duration.Inf
import akka.dispatch.{Await, ExecutionContext, Future}
import collection.parallel.ForkJoinTasks
import java.util.concurrent.{ExecutorService, Executor, Executors}

object ParallelVsSequence extends App {
  val nProcessors = Runtime.getRuntime.availableProcessors

  /* A new implementation of ForkJoin will be embedded in Scala 2.10 and Akka will use that.
   * This constructor was deprecated in Akka 2.0.2, but there is no alternative for Scala 2.9.x
   * Unfortunately, there is currently no way to suppress ScalaDoc deprecation warnings. */
  val esFJP = new ForkJoinPool()
  val esFTP = Executors.newFixedThreadPool(nProcessors)
  val esCTP = Executors.newCachedThreadPool()
  val esSTE = Executors.newSingleThreadExecutor()

  private val configString: String = """akka {
    logConfigOnStart=off
    executor = "fork-join-executor"
    fork-join-executor {
        parallelism-min = %d
        parallelism-factor = 1.0
        parallelism-max = %d
    }
  }""".format(nProcessors, nProcessors)
  private val system = ActorSystem.apply("default", ConfigFactory.parseString(configString))

  ForkJoinTasks.defaultForkJoinPool.setParallelism(nProcessors)
  implicit var dispatcher: ExecutionContext = null
  val ecNameMap = scala.collection.immutable.Map(
    system -> "akka.actor.ActorSystem",
    esFJP  -> "akka.jsr166y.ForkJoinPool",
    esFTP  -> "FixedThreadPool (java.util.concurrent.ThreadPoolExecutor)",
    esCTP  -> "CachedThreadPool (java.util.concurrent.ThreadPoolExecutor)",
    esSTE  -> "SingleThreadExecutor (java.util.concurrent.ThreadPoolExecutor)"
  )


  List(system, esFJP, esFTP, esCTP, esSTE).foreach { e: Any =>
    if (e==system) {
      dispatcher = system.dispatcher
      doit(ecNameMap.get(e.asInstanceOf[AnyRef]).get)
      system.shutdown()
    } else {
      dispatcher = ExecutionContext.fromExecutor(e.asInstanceOf[Executor])
      doit(ecNameMap.get(e.asInstanceOf[AnyRef]).get)
      e.asInstanceOf[ExecutorService].shutdown()
    }
  }


  def doit(executorName: String) = {
    println("Warming up hotspot to test " + executorName)
    parallelTest
    Await.ready(futureTest, Inf)
    println("\nRunning tests on " + executorName)
    parallelTest
    Await.ready(futureTest, Inf)
    println("\n---------------------------------------------------\n")
  }

  def futureTest = {
    val t0 = System.nanoTime()
    val futures = time {
      for (i <- 1 to NumInterations) yield Future { expensiveCalc }
    }("Futures creation time")

    Future sequence futures andThen { case f =>
      val t1 = System.nanoTime()
      println("Total time for Akka future version: "+ (t1 - t0)/1000000 + "ms")
      f match {
        case Right(result) =>
          println("Result using Akka future version: " + result.sum.asInstanceOf[Int])
        case Left(exception) =>
          println(exception.getMessage)
      }
    }
  }

  def parallelTest = time {
    val parallelResult = time {
      (1 to NumInterations).par.view.map { x => expensiveCalc }
    }("Parallel collection elapsed time")
    println("Result using Scala parallel collections: " + parallelResult.sum.asInstanceOf[Int])
  }("Parallel view resolution")
}