package com.micronautics.akka.dispatch.futureScala

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.dispatch.{MessageDispatcher, Future}
import java.net.URL
import scalax.io.Codec
import scalax.io.JavaConverters.asInputConverter

object Fold extends App {
  private val configString: String = "akka { logConfigOnStart=off }"
  private val system: ActorSystem = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString))
  implicit private var dispatcher: MessageDispatcher = system.dispatcher

  val futures = List(
    Future(httpGet("http://akka.io")),
    Future(httpGet("http://www.playframework.org")),
    Future(httpGet("http://nbronson.github.com/scala-stm"))
  )
  val f = Future.fold(futures)(0) { (sum:Int, page:String) =>
    println("fold closure invoked")
    sum + page.length
  } onComplete { // try
    case Right(result)   => system.log.info("Fold Scala result: " + result)
    case Left(exception) => system.log.info("Fold Scala exception: " + exception.getMessage)
  } recover  {   // catch
    case e:Exception => system.log.info(e.getMessage)
  } andThen {    // finally
    case _ =>
      system.log.info("Shutting down")
      system.shutdown()
  }

  /** Fetches contents of web page pointed to by urlStr */
  def httpGet(urlStr:String):String =
    new URL(urlStr).asInput.string(Codec.UTF8)
}