package com.micronautics.akka

/**
 * @author Mike Slinn
 * @author calculatePiFor() provided by Typesafe
 */

class ExpensiveCalc {}

object ExpensiveCalc {
  val NumInterations = 1000

  def expensiveCalc = calculatePiFor(0, 1000000)

  def calculatePiFor(start: Int, nrOfElements: Int): Double = {
    var acc = 0.0
    for (i <- start until (start + nrOfElements))
      acc += 4.0 * (1 - (i % 2) * 2) / (2 * i + 1)
    acc
  }

  def time[R](block: => R)(msg: String="Elapsed time"): R = {
      val t0 = System.nanoTime()
      val result = block
      val t1 = System.nanoTime()
      println(msg + ": "+ (t1 - t0)/1000000 + "ms")
      result
  }
}
