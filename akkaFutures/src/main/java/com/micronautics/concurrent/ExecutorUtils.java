package com.micronautics.concurrent;

import akka.dispatch.ExecutionContext;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.OnComplete;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Mike Slinn */
public class ExecutorUtils {

    /** Wait until all futures have completed, then exit() */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public static void exitWhenAllDone(ExecutionContext context, Future<?>... futuresIn) {
        List<Future<Object>> futures = new LinkedList<Future<Object>>();
        for (Future future : futuresIn)
            futures.add(future);
        Future<Iterable<Object>> g = Futures.sequence(futures, context);
        g.onComplete(new OnComplete<Iterable<Object>>() {
            @Override
			public void onComplete(final Throwable throwable, final Iterable result) {
                System.exit(0);
            }
        });
    }
}
