package com.micronautics.akka.dispatch.futureJava;

import akka.dispatch.ExecutionContext;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.OnComplete;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Mike Slinn */
public class FuturePropertyDemo {
    private final ExecutorService executorService = Executors.newFixedThreadPool(10);

    private final ExecutionContext context = new ExecutionContext() {
        @Override public void execute(final Runnable runnable) { executorService.execute(runnable); }

        public void reportFailure(Throwable throwable) { System.err.println(throwable); }
    };

    private final Callable<Integer> callable = new Callable<Integer>() {
        @Override public Integer call() {
            try {
                Thread.sleep(6000);
            } catch (InterruptedException ignored) { }
            System.out.println("Thread is done");
            return 2 + 3;
        }
    };

    private final OnComplete<Integer> completionFn = new OnComplete<Integer>() {
        @Override public void onComplete(final Throwable throwable, final Integer result) {
            if (result != null) {
                System.out.println("Completed result: " + result);
            } else {
                System.out.println("Exception: " + throwable);
            }
        }
    };

    private final OnComplete<Integer> shutdownFn = new OnComplete<Integer>() {
        @Override public void onComplete(final Throwable throwable, final Integer result) {
            executorService.shutdown(); // allows program to System.exit()
        }
    };


    private void doit() {
        final Future<Integer> f = Futures.future(callable, context);
        f.andThen(completionFn).andThen(shutdownFn);
        System.out.println("FuturePropertyDemo Java: Is thread done yet? " + f.isCompleted());
        System.out.println("Uncompleted thread value: " + f.value());
    }

    public static void main(final String[] args) {
        new FuturePropertyDemo().doit();
    }
}
