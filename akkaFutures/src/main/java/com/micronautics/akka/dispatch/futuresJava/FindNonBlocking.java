package com.micronautics.akka.dispatch.futuresJava;

import akka.dispatch.ExecutionContext;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.OnComplete;
import akka.japi.Function2;
import com.micronautics.util.HttpGetter;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**Print URLs of web pages that contain the string {{{Scala}}}. */
class FindNonBlocking {
    private final ExecutorService executorService = Executors.newFixedThreadPool(10);

    private final ExecutionContext context = new ExecutionContext() {
        @Override public void execute(Runnable runnable) { executorService.execute(runnable); }

        public void reportFailure(Throwable throwable) { System.err.println(throwable); }
    };

    private final ArrayList<Future<String>> futures = new ArrayList<Future<String>>();

    protected final ArrayList<String> initialValue = new ArrayList<String>();

    private final Function2<ArrayList<String>, String, ArrayList<String>> applyFunction = new Function2<ArrayList<String>, String, ArrayList<String>>() {
        @Override
		public ArrayList<String> apply(final ArrayList<String> result, final String contents) {
            if (contents.indexOf("Scala")>0)
                result.add(contents);
            return result;
        }
    };

	private final OnComplete<ArrayList<String>> completionFunction = new OnComplete<ArrayList<String>>() {

    	/** This method is executed asynchronously, probably after the mainline has completed */
        @Override
		public void onComplete(final Throwable throwable, final ArrayList<String> result) {
            if (result != null) {
                System.out.println("Nonblocking Java fold: " + result.size() + " web pages contained 'Scala'.");
            } else {
                System.out.println("Exception: " + throwable);
            }
        }
    };

    private final OnComplete<ArrayList<String>> shutdownFunction = new OnComplete<ArrayList<String>>() {
        @Override public void onComplete(final Throwable exception, final ArrayList<String> result) {
            executorService.shutdown(); // releases threadpool resources so System.exit() can run
        }
    };


    {   /* Build array of Futures that will run on regular threads. Remember that HttpGetter implements Callable */
    	futures.add(Futures.future(new HttpGetter("http://akka.io/"), context));
        futures.add(Futures.future(new HttpGetter("http://www.playframework.org/"), context));
        futures.add(Futures.future(new HttpGetter("http://nbronson.github.com/scala-stm/"), context));
    }


    public void doit() {
        final Future<ArrayList<String>> resultFuture = Futures.fold(initialValue, futures, applyFunction, context);
        resultFuture.andThen(completionFunction).andThen(shutdownFunction);
    }

    public static void main(final String[] args) {
        new FindNonBlocking().doit();
    }
}