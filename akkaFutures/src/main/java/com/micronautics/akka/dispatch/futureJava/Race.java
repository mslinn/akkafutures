package com.micronautics.akka.dispatch.futureJava;

import akka.actor.ActorSystem;
import akka.dispatch.Await;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.MessageDispatcher;
import akka.dispatch.OnComplete;
import akka.util.Duration;
import com.typesafe.config.ConfigFactory;

import java.util.concurrent.Callable;

class Race {
    private final String configString = "akka { logConfigOnStart=off }";
    ActorSystem system = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString));
    MessageDispatcher dispatcher = system.dispatcher();

	private volatile int offset = 6;

    /** onComplete handler for nonblocking version */
	private final OnComplete<Integer> completionFn = new OnComplete<Integer>() {

    	/** This method is executed asynchronously, probably after the mainline has completed */
        @Override
		public void onComplete(final Throwable throwable, final Integer result) {
        	System.out.println("Race Java entered onComplete, offset = " + offset);
            if (result != null) {
                System.out.println("Race Java result: " + result);
            } else {
                System.out.println("Race exception: " + throwable);
            }
        }
    };

    private final Callable<Integer> callableVar = new Callable<Integer>() {
        @Override
		public Integer call() {
            return 2 + 3 + offset;
        }
    };

    private final Callable<Integer> callableAccessor = new Callable<Integer>() {
        @Override
		public Integer call() {
            return 2 + 3 + accessor();
        }
    };


    private int accessor() { return offset; }

    public void doit() {
        final Future<Integer> resultVar = Futures.future(callableVar, dispatcher);
        final Future<Integer> resultAccessor = Futures.future(callableAccessor, dispatcher);
        offset = 42;
        resultVar.onComplete(completionFn);
        resultAccessor.onComplete(completionFn);

        try { // block until both futures have completed, then shut down
            Await.ready(resultVar, Duration.Inf());
            Await.ready(resultAccessor, Duration.Inf());
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
        system.shutdown();
    }

    public static void main(final String[] args) {
        new Race().doit();
    }
}