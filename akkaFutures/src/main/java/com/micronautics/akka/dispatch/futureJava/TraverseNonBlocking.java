package com.micronautics.akka.dispatch.futureJava;

import akka.actor.ActorSystem;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.MessageDispatcher;
import akka.dispatch.OnComplete;
import akka.japi.Function;
import com.micronautics.util.HttpGetter;
import com.typesafe.config.ConfigFactory;

import java.util.ArrayList;
import java.util.Arrays;

/** This example uses traverse() to print URLs of web pages that contain the string {{{Scala}}}.
 * Non-blocking fold is executed on the thread of the last Future to be completed.
 * If this Future is completed with an exception then the new Future will also contain this exception.
 *
 * The call to Futures.future() forwards the HttpGetter Callables to Future.apply(), part of the Scala API.
 * They are then sent to the ExecutionContext to be run.
 * If the ExecutionContext is an Akka dispatcher then it does some additional preparation before queuing the futures for processing.
 * Futures.traverse() registers callback functions with each of the futures in order to collect all the results needed in order to produce the final result using applyFunction().
 *
 * The Callable is not in scope of applyFunction2.apply(), unlike some other composable functions.
 * That means that public properties from cannot be retrieved from the Callable.
 * If this is important, HttpGetter.call() should be modified to return a result object, perhaps a HashMap, that wraps the url and the resulting content.
 * @see https://github.com/jboner/akka/blob/releasing-2.0-M2/akka-docs/java/code/akka/docs/future/FutureDocTestBase.java */
class TraverseNonBlocking {
    private final String configString = "akka { logConfigOnStart=off }";
    ActorSystem system = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString));
    MessageDispatcher dispatcher = system.dispatcher();

    /** Collection of String, which Futures.traverse will turn into a Future of a collection.
     * The futures will run under a regular context. */
    private final ArrayList<String> urls = new ArrayList<String> (Arrays.asList(
        "http://akka.io/",
        "http://www.playframework.org/",
        "http://nbronson.github.com/scala-stm/"
    ));

    /** Composable function for both versions */
    private final Function<String, Future<String>> traverseFn = new Function<String, Future<String>>() {
        @Override
		public Future<String> apply(final String url) {
            return Futures.future(new HttpGetter(url, "Scala"), dispatcher);
        }
    };

    /** onComplete handler for nonblocking version */
    private final OnComplete<Iterable<String>> completionFn = new OnComplete<Iterable<String>>() {

    	/** This method is executed asynchronously, probably after the mainline has completed */
		@Override
		public void onComplete(final Throwable throwable, final Iterable<String> result) {
            if (result != null) {
                int matchCount = 0;
                for (String r : result)
                	if (r.length()>0)
                		matchCount++;
                System.out.println("Nonblocking Java traverse: " + matchCount + " web pages contained 'Scala'.");
            } else {
                System.out.println("Nonblocking Java traverse exception: " + throwable);
            }
        }
    };

    private final OnComplete<Iterable<String>> shutdownFn = new OnComplete<Iterable<String>>() {
        @Override public void onComplete(final Throwable throwable, final Iterable<String> result) {
            System.out.println("Shutting down");
            system.shutdown(); // allows program to System.exit()
        }
    };


    /** Demonstrates how to invoke traverse() asynchronously.
     * Regular threads are used, because execution continues past onComplete(), and the callback to onComplete()
     * needs to be available after the main program has finished execution. If daemon threads were used, the program
     * would exit before the onComplete() callback was invoked. This means that onComplete() must contain a means of
     * terminating the program, or setting up another callback for some other purpose. The program could be terminated
     * with a call to System.exit(0), or by invoking executorService.shutdown() to shut down the thread. */
    public void doit() {
        final Future<Iterable<String>> future = Futures.traverse(urls, traverseFn, dispatcher);
        future.andThen(completionFn).andThen(shutdownFn);
    }

    public static void main(final String[] args) {
        new TraverseNonBlocking().doit();
    }
}