package com.micronautics.akka.dispatch.futureJava;

import akka.dispatch.*;
import com.micronautics.util.HttpGetter;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class SequenceNonBlocking {
    /** executorService creates regular threads, which continue running when the application tries to exit. */
    private final ExecutorService executorService = Executors.newFixedThreadPool(10);

    /** Akka uses the execution context to manage futures under its control. This ExecutionContext creates regular threads. */
    private final ExecutionContext context = new ExecutionContext() {
        @Override public void execute(Runnable r) { executorService.execute(r); }

        public void reportFailure(Throwable throwable) { System.err.println(throwable); }
    };

    /** Collection of futures, which Futures.sequence will turn into a Future of a collection.
     * These futures will run under a regular context. */
    private final LinkedList<Future<String>> futures = new LinkedList<Future<String>>();

    /** Composable function for both versions */
    private final Mapper<Iterable<String>, Iterable<String>> mapFn = new Mapper<Iterable<String>, Iterable<String>>() {
		public List<String> apply(final Iterable<String> contents) {
            System.out.println("SequenceNonBlocking mapFn thread " + Thread.currentThread().getName());
        	final List<String> result = new LinkedList<String>();
        	for (final String content : contents)
        	    if (content.indexOf("Scala")>0)
        	    	result.add(content);
        	return result;
        }
    };

    private final OnComplete<Iterable<String>> completionFn = new OnComplete<Iterable<String>>() {

    	/** This method is executed asynchronously, probably after the mainline has completed
         * @param result Scala's default implementation of Iterable<T> is LinkedList<T> */
        @Override
		public void onComplete(final Throwable throwable, final Iterable<String> result) {
            System.out.println("SequenceNonBlocking onComplete thread " + Thread.currentThread().getName());
            if (result != null) {
                System.out.println("Nonblocking Java sequence: " + ((LinkedList<String>)result).size() + " web pages contained 'Scala'.");
            } else {
                System.out.println("Exception: " + throwable);
            }
        }
    };

    private final OnComplete<Iterable<String>> shutdownFn = new OnComplete<Iterable<String>>() {
        @Override public void onComplete(final Throwable throwable, final Iterable<String> result) {
            System.out.println("Shutting down");
            executorService.shutdown(); // allows program to System.exit()
        }
    };


    {   /* Build array of Futures that will run on regular threads. Remember that HttpGetter implements Callable */
    	futures.add(Futures.future(new HttpGetter("http://akka.io/"), context));
        futures.add(Futures.future(new HttpGetter("http://www.playframework.org/"), context));
        futures.add(Futures.future(new HttpGetter("http://nbronson.github.com/scala-stm/"), context));
    }


    /** Demonstrates how to invoke sequence() asynchronously.
     * Regular threads are used, because execution continues past onComplete(), and the callback to onComplete()
     * needs to be available after the main program has finished execution. If daemon threads were used, the program
     * would exit before the onComplete() callback was invoked. This means that onComplete() must contain a means of
     * terminating the program, or setting up another callback for some other purpose. The program could be terminated
     * with a call to System.exit(0), or by invoking executorService.shutdown() to shut down the thread. */
    public void doit() {
        final Future<Iterable<String>> future1 = Futures.sequence(futures, context);//.mapTo(manifest(Future<List<String>>>.class));
        final Future<Iterable<String>> future2 = future1.map(mapFn);
        future2.andThen(completionFn).andThen(shutdownFn);
    }

    public static void main(final String[] args) {
        new SequenceNonBlocking().doit();
    }
}