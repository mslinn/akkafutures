package com.micronautics.akka.dispatch.futureJava;


import akka.dispatch.*;
import scala.Left;
import scala.Right;

import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static akka.japi.Util.manifest;

/**
 * This example uses Future.future() to create a future that computes 2+3.
 */
class FlatMapJava {
    /**
     * executorService creates regular threads, which continue running when the application tries to exit.
     */
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    /**
     * Akka uses the execution context to manage futures under its control. This ExecutionContext creates regular threads.
     */
    private final ExecutionContext context = new ExecutionContext() {
        @Override
        public void execute(final Runnable runnable) {
            executorService.execute(runnable);
        }

        public void reportFailure(Throwable throwable) {
            System.err.println(throwable);
        }
    };

    private CountDownLatch countDown = new CountDownLatch(2); // 2 users to authenticate in this demo


    /**
     * Hard-coded for simplicity
     *
     * @return Future containing user id or an Exception.
     */
    Future<Long> authenticate(final String token) {
        Promise<Long> p = Futures.promise(context);
        if (token.compareTo("asdf") == 0)
            p.tryComplete(new Right<Throwable, Long>(123456L));
        else
            p.tryComplete(new Left<Throwable, Long>(new Exception("Invalid token " + token)));
        return p;
    }

    /**
     * @return Successful future containing User, otherwise short circuit and
     *         return failed Future if auth() fails
     */
    Future<User> lookupUser(final String token) {
        Future<Long> fId = authenticate(token);
        Future<User> fUser = fId.flatMap(new Mapper<Long, Future<User>>() {
            public Future<User> apply(final Long id) {
                System.out.println("lookupUser(" + token + ") authenticated with id=" + id);
                Future<User> fUser = Futures.future(new GetUser(id), context).mapTo(manifest(User.class));
                return fUser;
            }
        });
        return fUser;
    }

    void loginTest(final String token) {
        Future<User> user = lookupUser(token);
        user.onComplete(new OnComplete<User>() {
            @Override
            public void onComplete(final Throwable exception, final User user) {
                if (user != null) {
                    System.out.println("FlatMapJava user: " + user);
                } else {
                    System.out.println("FlatMapJava exception: " + exception.getMessage());
                }
                countDown.countDown();
                if (countDown.getCount() == 0)
                    executorService.shutdown();
            }
        });
    }

    public void doit() {
        loginTest("asdf");
        loginTest("nope");
    }

    public static void main(final String[] args) {
        new FlatMapJava().doit();
    }


    private class GetUser implements Callable<User> {
        private Long id;

        public GetUser(final Long id) {
            this.id = id;
        }

        @Override
        public User call() throws Exception {
            if (id == 123456)
                return new User("Fred", "Flintstone", new Date(475488000000L));
            else
                throw new Exception("No user with id " + id);
        }
    }

    private class User {
        private String firstName;
        private String lastName;
        private Date signedUp;

        public User(final String firstName, final String lastName, final Date signedUp) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.signedUp = signedUp;
        }

        public String toString() {
            return "User " + firstName + " " + lastName + " " + signedUp;
        }
    }
}
