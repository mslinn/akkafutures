package com.micronautics.akka.dispatch.futureJava;

import akka.actor.ActorSystem;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.MessageDispatcher;
import akka.dispatch.OnComplete;
import com.typesafe.config.ConfigFactory;

import java.util.concurrent.Callable;

/** This example uses Future.future() to create a future that computes 2+3. */
class OnCompleteJava {
    private final String configString = "akka { logConfigOnStart=off }";
    ActorSystem system = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString));
    MessageDispatcher dispatcher = system.dispatcher();

    private final OnComplete<Integer> completionFn = new OnComplete<Integer>() {
        @Override
		public void onComplete(final Throwable exception, final Integer result) {
            if (result != null) {
                System.out.println("OnCompleteJava result: " + result);
            } else {
                System.out.println("OnCompleteJava exception: " + exception);
            }
        }
    };

    private final OnComplete<Integer> shutdownFn = new OnComplete<Integer>() {
        @Override public void onComplete(final Throwable exception, final Integer result) {
            system.shutdown(); // releases threadpool resources so System.exit() can run
        }
    };

    private final Callable<Integer> callable = new Callable<Integer>() {
        @Override public Integer call() { return 2 + 3; }
    };


    public void doit() {
        final Future<Integer> f = Futures.future(callable, dispatcher);
        // The onComplete() might execute after andThen():
        //f.onComplete(completionFn).andThen(shutdownFn);
        f.andThen(completionFn).andThen(shutdownFn);
    }

    public static void main(final String[] args) {
        new OnCompleteJava().doit();
    }
}