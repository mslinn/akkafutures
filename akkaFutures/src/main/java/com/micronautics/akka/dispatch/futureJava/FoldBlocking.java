package com.micronautics.akka.dispatch.futureJava;

import akka.actor.ActorSystem;
import akka.dispatch.Await;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.MessageDispatcher;
import akka.japi.Function2;
import akka.util.Duration;
import com.micronautics.util.HttpGetter;
import com.typesafe.config.ConfigFactory;

import java.util.ArrayList;
import java.util.Arrays;

import static java.util.concurrent.TimeUnit.SECONDS;

/** Print URLs of web pages that contain the string {{{Scala}}}. */
class FoldBlocking {
    private final String configString = "akka { daemonic = on }";
    ActorSystem system = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString));
    MessageDispatcher dispatcher = system.dispatcher();

    /** Collection of futures, which Futures.sequence will turn into a Future of a collection.
     * Remember that HttpGetter implements Callable. These futures will be backed by daemon threads. */
	private final ArrayList<Future<String>> daemonFutures = new ArrayList<Future<String>> (Arrays.asList(
            Futures.future(new HttpGetter("http://akka.io/"), dispatcher),
            Futures.future(new HttpGetter("http://www.playframework.org/"), dispatcher),
            Futures.future(new HttpGetter("http://nbronson.github.com/scala-stm/"), dispatcher)
        ));

    /** Accumulates result during fold(), also provides initial results, if desired. */
    protected final ArrayList<String> initialValue = new ArrayList<String>();

    /** Composable function for both versions */
    private final Function2<ArrayList<String>, String, ArrayList<String>> applyFn = new Function2<ArrayList<String>, String, ArrayList<String>>() {
        @Override
		public ArrayList<String> apply(final ArrayList<String> result, final String contents) {
            if (contents.contains("Scala"))
                result.add(contents);
            return result;
        }
    };


    public void doit() {
        final Future<ArrayList<String>> f = Futures.fold(initialValue, daemonFutures, applyFn, dispatcher);
        try { // Await.result() blocks until the Future completes
            final ArrayList<String> result = Await.result(f, Duration.create(10, SECONDS));
            System.out.println("Blocking Java fold: " + result.size() + " web pages contained 'Scala'.");
        } catch (Exception ignored) {}
    	System.out.println("End of mainline");
    }

    /** Demonstrates how to invoke fold() and block until a result is available */
    public static void main(final String[] args) {
    	new FoldBlocking().doit();
    }
}