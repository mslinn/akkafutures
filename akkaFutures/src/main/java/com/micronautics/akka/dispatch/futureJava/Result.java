package com.micronautics.akka.dispatch.futureJava;

import akka.actor.ActorSystem;
import akka.dispatch.*;
import akka.util.Duration;
import com.typesafe.config.ConfigFactory;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeoutException;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

/** Await.result() registers callback functions with each of the futures in order to collect all the results needed in
 * order to produce the final result using applyFunction(). */
class Result {
    private final String configString = "akka { logConfigOnStart=off }";
    ActorSystem system = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString));
    MessageDispatcher dispatcher = system.dispatcher();

    /** Maximum length of time to block while waiting for future to complete */
    private final Duration timeout = Duration.create(5, MILLISECONDS);

    private final Callable<Integer> callable = new Callable<Integer>() {
        @Override
        public Integer call() throws InterruptedException {
            Thread.sleep(1000); // comment this out to obtain the returned value
            return 2 + 3;
        }
    };


    public void doit() {
        final Future<Integer> resultFuture = Futures.future(callable, dispatcher);
        try {
            // Await.result() blocks until the Future completes
            final Integer result = Await.result(resultFuture, timeout);
            System.out.println("Java result(): " + result);
        } catch (Exception ex) { // TimeoutException is not expected by compiler
            if (ex instanceof TimeoutException)
                System.err.println("TimeoutException");
            else
                System.err.println(ex.getMessage());
        }
    }

    public static void main(final String[] args) {
        Result resultBlocking = new Result();
        resultBlocking.doit();
        resultBlocking.system.shutdown();
    }
}