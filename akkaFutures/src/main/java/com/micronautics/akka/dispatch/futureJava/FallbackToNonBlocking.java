package com.micronautics.akka.dispatch.futureJava;

import akka.dispatch.ExecutionContext;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.OnComplete;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class FallbackToNonBlocking {
    private final ExecutorService executorService = Executors.newFixedThreadPool(10);

private final ExecutionContext context = new ExecutionContext() {
    @Override public void execute(final Runnable runnable) { executorService.execute(runnable); }

    public void reportFailure(Throwable throwable) { System.err.println(throwable); }
};

    private final OnComplete<Integer> completionFn = new OnComplete<Integer>() {
        @Override public void onComplete(final Throwable exception, final Integer result) {
            if (result != null) {
                System.out.println("Nonblocking Java FallbackTo result: " + result);
            } else {
                System.out.println("Nonblocking Java FallbackTo exception: " + exception);
            }
        }
    };

    private final OnComplete<Integer> shutdownFn = new OnComplete<Integer>() {
        @Override public void onComplete(final Throwable exception, final Integer result) {
            executorService.shutdown(); // releases threadpool resources so System.exit() can run
        }
    };

    private final Callable<Integer> callable5 = new Callable<Integer>() {
        @Override public Integer call() { return 2 + 3; }
    };

    private final Callable<Integer> callableException = new Callable<Integer>() {
        /* throws ArithmeticException */
        @Override public Integer call() { return 6 / 0; }
    };


    public void doit() {
        final Future<Integer> futureException = Futures.future(callableException, context);
        final Future<Integer> future5         = Futures.future(callable5,         context);
        final Future<Integer> futureOr        = futureException.fallbackTo(future5);
        futureOr.andThen(completionFn).andThen(shutdownFn);
    }

    public static void main(final String[] args) {
        new FallbackToNonBlocking().doit();
    }
}