package com.micronautics.akka.dispatch;

import akka.actor.ActorSystem;
import akka.dispatch.Await;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.MessageDispatcher;
import akka.util.Duration;
import com.typesafe.config.ConfigFactory;

import java.util.concurrent.Callable;

/**Configure Akka system with an Executor that spins daemon threads */
class ConfigJavaDaemon {
    private final String configString = "akka { daemonic = on }";
    ActorSystem system = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString));
    MessageDispatcher dispatcher = system.dispatcher();

    private final Callable<Integer> callable = new Callable<Integer>() {
        @Override public Integer call() { return 2 + 3; }
    };

    public void doit() {
        final Future<Integer> resultFuture = Futures.future(callable, dispatcher);
        // IntelliJ IDEA highlights Inf() because Scala traits are easily accessible from Java
        // but the code compiles and runs
        try {
            Await.ready(resultFuture, Duration.Inf());
            System.out.println("ConfigJavaDaemon result: " + resultFuture.value().get().right().get());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        system.shutdown();
    }

    public static void main(final String[] args) {
        new ConfigJavaDaemon().doit();
    }
}