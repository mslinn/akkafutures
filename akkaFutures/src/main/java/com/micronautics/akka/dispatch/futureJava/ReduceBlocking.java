package com.micronautics.akka.dispatch.futureJava;

import akka.actor.ActorSystem;
import akka.dispatch.Await;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.MessageDispatcher;
import akka.japi.Function2;
import akka.util.Duration;
import com.typesafe.config.ConfigFactory;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import static java.util.concurrent.TimeUnit.SECONDS;

/** Uses reduce() to sum the results of a collection of Futures of an expensive computation. */
class ReduceBlocking {
    private final String configString = "akka { daemonic = on }";
    ActorSystem system = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString));
    MessageDispatcher dispatcher = system.dispatcher();

    /** Collection of futures, which Futures.reduce will process and obtain a value from.
     * These futures will run under daemonContext. */
    private final ArrayList<Future<Long>> daemonFutures = new ArrayList<Future<Long>>();

    /** Composable function for both versions. */
    private final Function2<Long, Long, Long> sum = new Function2<Long, Long, Long>() {
    	@Override
		public Long apply(final Long result, final Long item) { return result + item; }
    };


    {   /* Build array of Futures that will run on daemon threads. ExpensiveCalc implements Callable */
    	for (int i=1; i<=100; i++)
    	    daemonFutures.add(Futures.future(new ExpensiveCalc(i), dispatcher));
    }


    public void doit() {
        final Future<Long> resultFuture = Futures.reduce(daemonFutures, sum, dispatcher);
        try { // Await.result() blocks up to 10 seconds or until the Future completes
            final Long result = Await.result(resultFuture, Duration.create(10, SECONDS));
            System.out.println("Blocking Java reduce result: " + result);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }

    public static void main(final String[] args) {
    	new ReduceBlocking().doit();
    }

    private class ExpensiveCalc implements Callable<Long> {
        private final Integer x;

    	ExpensiveCalc(final Integer x) { this.x = x; }

    	/** Pretend this method consumes a lot of computational resource */
    	@Override public Long call() { return 1L * x * x; }
    }
}