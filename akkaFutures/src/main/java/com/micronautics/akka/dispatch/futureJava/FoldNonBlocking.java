package com.micronautics.akka.dispatch.futureJava;

import akka.dispatch.ExecutionContext;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.OnComplete;
import akka.dispatch.Recover;
import akka.japi.Function2;
import com.micronautics.util.HttpGetter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class FoldNonBlocking {
    private final ExecutorService executorService = Executors.newFixedThreadPool(10);

    private final ExecutionContext context = new ExecutionContext() {
        @Override public void execute(final Runnable runnable) { executorService.execute(runnable); }

        public void reportFailure(Throwable throwable) { System.err.println(throwable); }
    };

    /** Collection of futures, which Future.fold() will walk through to aggregate a result.
     * These futures will run under a regular context. */
	private final ArrayList<Future<String>> futures = new ArrayList<Future<String>> (Arrays.asList(
        // Build array of Futures that will run on regular threads. Remember that HttpGetter implements Callable
        Futures.future(new HttpGetter("http://akka.io"), context),
        Futures.future(new HttpGetter("http://www.playframework.org/"), context),
        Futures.future(new HttpGetter("http://nbronson.github.com/scala-stm/"), context)
    ));

    /** Initial value for the fold, also known as zero value. */
    protected final Integer initialValue = 0;

    /** This method will be called repeatedly by fold for each future passed in,
     * unless one of the futures is completed with an exception */
    private final Function2<Integer, String, Integer> foldFn = new Function2<Integer, String, Integer>() {
		public Integer apply(final Integer zero, final String contents) {
            return zero + contents.length();
        }
    };

    private final OnComplete<Integer> completionFn = new OnComplete<Integer>() {
        @Override public void onComplete(final Throwable throwable, final Integer result) {
            if (throwable == null) {
                System.out.println("Nonblocking Java fold: " + result);
            } else {
                System.out.println("Nonblocking Java exception: " + throwable.getMessage());
            }
        }
    };

    private final Recover<Integer> recoverFn = new Recover<Integer>() {
        @Override public Integer recover(final Throwable throwable) {
            System.err.println("Recover: " + throwable.getMessage());
            return 0;
        }
    };

    private final OnComplete<Integer> shutdownFn = new OnComplete<Integer>() {
        @Override public void onComplete(final Throwable throwable, final Integer result) {
            System.out.println("Shutting down");
            executorService.shutdown(); // allows program to System.exit()
        }
    };

    // uncomment the next two lines to see what happens when a future fails
    //Future<String> failed = Futures.failed(new Exception("Deliberately failed future"), context);
    //futures.add(failed);

    public void doit() {
        final Future<Integer> f = Futures.fold(initialValue, futures, foldFn, context);
        f.onComplete(completionFn).recover(recoverFn).andThen(shutdownFn);
        System.out.println("End of mainline");
    }

    public static void main(final String[] args) {
        new FoldNonBlocking().doit();
    }
}