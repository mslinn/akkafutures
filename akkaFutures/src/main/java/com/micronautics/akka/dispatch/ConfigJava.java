package com.micronautics.akka.dispatch;

import akka.actor.ActorSystem;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.MessageDispatcher;
import akka.dispatch.OnComplete;

import java.util.concurrent.Callable;

/** Configure Akka system from src/main/resources/application.conf */
class ConfigJava {
    ActorSystem system = ActorSystem.apply();
    MessageDispatcher dispatcher = system.dispatcher();

    private final OnComplete<Integer> completionFn = new OnComplete<Integer>() {
        @Override
		public void onComplete(final Throwable exception, final Integer result) {
            if (result != null) {
                System.out.println("ConfigJava result: " + result);
            } else {
                System.out.println("ConfigJava exception: " + exception);
            }
        }
    };

    private final OnComplete<Integer> shutdownFn = new OnComplete<Integer>() {
        @Override public void onComplete(final Throwable exception, final Integer result) {
            system.logConfiguration(); // outputs configuration info
            System.out.println("ActorSystem name=" + system.name());
            System.out.println("ConfigJava before shutdown: actorSystem isTerminated=" + system.isTerminated());
            system.shutdown(); // allow System.exit()
            System.out.println("ConfigJava after shutdown: actorSystem isTerminated=" + system.isTerminated());
            system.awaitTermination();
            System.out.println("ConfigJava after awaitTermination(): actorSystem isTerminated=" + system.isTerminated());
        }
    };

    private final Callable<Integer> callable = new Callable<Integer>() {
        @Override public Integer call() { return 2 + 3; }
    };


    public void doit() {
        final Future<Integer> resultFuture = Futures.future(callable, dispatcher);
        resultFuture.andThen(completionFn).andThen(shutdownFn);
    }

    public static void main(final String[] args) {
        new ConfigJava().doit();
    }
}