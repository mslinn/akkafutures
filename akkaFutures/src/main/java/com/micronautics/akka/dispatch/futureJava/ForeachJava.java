package com.micronautics.akka.dispatch.futureJava;

import akka.actor.ActorSystem;
import akka.dispatch.*;
import com.typesafe.config.ConfigFactory;

import java.util.concurrent.Callable;

/** This example uses Future.future() to create a future that computes 2+3. */
class ForeachJava {
    private final String configString = "akka { logConfigOnStart=off }";
    ActorSystem system = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString));
    MessageDispatcher dispatcher = system.dispatcher();

    private final Foreach<Integer> foreachFn = new Foreach<Integer>() {
		public void each(final Integer result) {
            System.out.println("Java foreach result: " + result);
        }
    };

    private final OnComplete<Integer> shutdownFn = new OnComplete<Integer>() {
        @Override public void onComplete(final Throwable exception, final Integer result) {
            system.shutdown(); // releases threadpool resources so System.exit() can run
        }
    };

    private final Callable<Integer> callable = new Callable<Integer>() {
        @Override public Integer call() { return 2 + 3; }
    };


    public void doit() {
        final Future<Integer> f = Futures.future(callable, dispatcher);
        f.foreach(foreachFn); // does not return anything
        f.andThen(shutdownFn);
    }

    public static void main(final String[] args) {
        new ForeachJava().doit();
    }
}