package com.micronautics.akka.dispatch.futureJava;

import akka.actor.ActorSystem;
import akka.dispatch.Await;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.MessageDispatcher;
import akka.util.Duration;
import com.typesafe.config.ConfigFactory;

import java.util.concurrent.Callable;

import static java.util.concurrent.TimeUnit.SECONDS;

/** Demonstrates how to invoke apply() and block until a result is available.
 * Await.result() registers callback functions with each of the futures in order to collect all the results needed in
 * order to produce the final result using applyFunction(). */
class ApplyBlocking {
    private final String configString = "akka { daemonic = on\n logConfigOnStart=off }";
    ActorSystem system = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString));
    MessageDispatcher dispatcher = system.dispatcher();

    private final Callable<Integer> callable = new Callable<Integer>() {
        @Override
		public Integer call() {
            return 2 + 3;
        }
    };


    public void doit() {
        final Future<Integer> resultFuture = Futures.future(callable, dispatcher);
        try {// Await.result() blocks up to 1 second, or until the Future completes
            final Integer result = Await.result(resultFuture, Duration.create(1, SECONDS));
            System.out.println("Blocking Java apply() result: " + result);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }

    public static void main(final String[] args) {
    	new ApplyBlocking().doit();
    }
}