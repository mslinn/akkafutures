package com.micronautics.akka.dispatch.futureJava;

import akka.actor.ActorSystem;
import akka.dispatch.Await;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.MessageDispatcher;
import akka.util.Duration;
import com.typesafe.config.ConfigFactory;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeoutException;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

class Ready {
    private final String configString = "akka { logConfigOnStart=off }";
    ActorSystem system = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString));
    MessageDispatcher dispatcher = system.dispatcher();

    /** Maximum length of time to block while waiting for future to complete */
    private final Duration timeout = Duration.create(5, MILLISECONDS);

    private final Callable<Integer> callable = new Callable<Integer>() {
        @Override
        public Integer call() throws InterruptedException {
            Thread.sleep(1000); // comment this out to obtain the returned value
            return 2 + 3;
        }
    };


    public void doit() {
        final Future<Integer> f1 = Futures.future(callable, dispatcher);
        try {
            final Future<Integer> f2 = Await.ready(f1, timeout);
            System.out.println("Java ready(): " + f2.value().get().right().get());
        } catch (Exception ex) { // TimeoutException is not expected by compiler
            if (ex instanceof TimeoutException)
                System.err.println("TimeoutException");
            else
                System.err.println(ex.getMessage());
        }
        system.shutdown();
    }

    public static void main(final String[] args) {
        new Ready().doit();
    }
}