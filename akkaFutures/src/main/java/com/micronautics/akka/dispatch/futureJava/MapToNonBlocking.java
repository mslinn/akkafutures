package com.micronautics.akka.dispatch.futureJava;

import akka.dispatch.ExecutionContext;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.OnComplete;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static akka.japi.Util.manifest;

class MapToNonBlocking {
    private final ExecutorService executorService = Executors.newFixedThreadPool(10);

    private final ExecutionContext context = new ExecutionContext() {
        @Override public void execute(final Runnable runnable) { executorService.execute(runnable); }

        public void reportFailure(Throwable throwable) { System.err.println(throwable); }
    };

    private final OnComplete<CharSequence> completionFn = new OnComplete<CharSequence>() {

        @Override
		public void onComplete(final Throwable throwable, final CharSequence result) {
            if (result != null) {
                System.out.println("Nonblocking Java mapTo result: " + result);
            } else {
                System.out.println("Nonblocking Java mapTo exception: " + throwable);
            }
        }
    };

    private final Callable<String> callableString = new Callable<String>() {
        @Override
		public String call() {
            return "asdf" + "qwer";
        }
    };

    private final OnComplete<CharSequence> shutdownFn = new OnComplete<CharSequence>() {
        @Override public void onComplete(final Throwable exception, final CharSequence result) {
            executorService.shutdown(); // releases threadpool resources so System.exit() can run
        }
    };

    public void doit() {
        final Future<CharSequence> future = Futures.future(callableString, context).mapTo(manifest(CharSequence.class));
        future.andThen(completionFn).andThen(shutdownFn);
    }

    public static void main(final String[] args) {
        new MapToNonBlocking().doit();
    }
}