package com.micronautics.akka.dispatch.futureJava;

import akka.dispatch.Await;
import akka.dispatch.ExecutionContext;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.util.Duration;
import com.micronautics.concurrent.DaemonExecutors;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

import static java.util.concurrent.TimeUnit.SECONDS;

class FallbackToBlocking {
    private final ExecutorService daemonExecutorService = DaemonExecutors.newFixedThreadPool(10);

    private final ExecutionContext context = new ExecutionContext() {
        @Override public void execute(final Runnable runnable) { daemonExecutorService.execute(runnable); }

		public void reportFailure(Throwable throwable) { System.err.println(throwable); }
    };

    private final Callable<Integer> callableInt = new Callable<Integer>() {
        @Override
		public Integer call() {
            return 2 + 3;
        }
    };

    private final Callable<Integer> callableInt2 = new Callable<Integer>() {
        @Override
		public Integer call() {
        	System.out.println("Evaluating callableInt2");
            return 2 + 3;
        }
    };

    private final Callable<String> callableString = new Callable<String>() {
        @Override
		public String call() {
            return "asdf" + "qwer";
        }
    };

    private final Callable<Integer> callableException = new Callable<Integer>() {
        @Override
		public Integer call() {
            return 6 / 0; // throws Exception
        }
    };


    public void doit() {
        final Future<Integer> futureException = Futures.future(callableException, context);
        @SuppressWarnings("unused")
        final Future<String>  futureString    = Futures.future(callableString,    context);
        final Future<Integer> futureInt       = Futures.future(callableInt,       context);
        final Future<Integer> futureInt2      = Futures.future(callableInt2,      context);
        // Not allowed; all fallbackTo futures must be of same type in Java
        //final Future<Integer> futureOr = futureException.or(futureString.or(futureInt));
        final Future<Integer> futureOr = futureException.fallbackTo(futureInt.fallbackTo(futureInt2));
        try {
            final Integer result = Await.result(futureOr, Duration.create(1, SECONDS));
            System.out.println("Blocking Java FallbackTo result: " + result);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }

    public static void main(final String[] args) {
        new FallbackToBlocking().doit();
    }
}