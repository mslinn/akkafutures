package com.micronautics.akka.dispatch.futureJava;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import akka.dispatch.ExecutionContext;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.OnComplete;
import akka.japi.Function2;

/** Invoke Future as a non-blocking function call, executed on another thread.
 * This example uses reduce to sum the results of a Future of an expensive computation.
 * Non-blocking reduce is executed on the thread of the last Future to be completed.
 *
 * The call to Futures.future() forwards the Callables to Future.apply(), part of the Scala API.
 * They are then sent to the ExecutionContext to be run.
 * If the ExecutionContext is an Akka dispatcher then it does some additional preparation before queuing the futures for processing.
 * Futures.reduce() registers callback functions with each of the futures in order to collect all the results needed in order to produce the final result using applyFunction().
 *
 * Future.reduce accepts a Function2[R, R, T].
 * @see https://github.com/jboner/akka/blob/releasing-2.0-M2/akka-docs/java/code/akka/docs/future/FutureDocTestBase.java */
class ReduceNonBlocking {
    /** executorService creates regular threads, which continue running when the application tries to exit. */
    private final ExecutorService executorService = Executors.newFixedThreadPool(10);

    /** Akka uses the execution context to manage futures under its control. This ExecutionContext creates regular threads. */
    private final ExecutionContext context = new ExecutionContext() {
        @Override public void execute(final Runnable runnable) { executorService.execute(runnable); }

        public void reportFailure(Throwable throwable) { System.err.println(throwable); }
    };

    /** Collection of futures, which Futures.sequence will turn into a Future of a collection.
     * These futures will run under a regular context. */
    private final ArrayList<Future<Long>> futures = new ArrayList<Future<Long>>();

    private final Function2<Long, Long, Long> sum = new Function2<Long, Long, Long>() {
        @Override public Long apply(final Long result, final Long item) { return result + item; }
    };

    private final OnComplete<Long> completionFunction = new OnComplete<Long>() {

    	/** This method is executed asynchronously, probably after the mainline has completed */
        @Override public void onComplete(final Throwable exception, final Long result) {
            if (result != null) {
                System.out.println("Nonblocking Java reduce result: " + result);
            } else {
                System.out.println("Nonblocking Java reduce exception: " + exception.getMessage());
            }
            executorService.shutdown(); // terminates program
        }
    };


    {   /* Build array of Futures that will run on regular threads. ExpensiveCalc implements Callable */
    	for (int i=1; i<=100; i++)
    	    futures.add(Futures.future(new ExpensiveCalc(i), context));
    }


    public void doit() {
        final Future<Long> f = Futures.reduce(futures, sum, context);
        f.andThen(completionFunction).andThen(shutdownFn);
    }

    private final OnComplete<Long> shutdownFn = new OnComplete<Long>() {
        @Override public void onComplete(final Throwable throwable, final Long result) {
            executorService.shutdown(); // allows program to System.exit()
        }
    };

    public static void main(String[] args) {
        new ReduceNonBlocking().doit();
    }


    private class ExpensiveCalc implements Callable<Long> {
        private final Integer x;

    	ExpensiveCalc(final Integer x) { this.x = x; }

    	/** Pretend this method consumes a lot of computational resource */
    	@Override
		public Long call() { return 1L * x * x; }
    }
}