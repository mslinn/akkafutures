package com.micronautics.akka.dispatch.futureJava;

import akka.dispatch.ExecutionContext;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.OnComplete;
import akka.japi.Function2;

import com.micronautics.util.HttpGetter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

// TODO rewrite this program so it actually uses map!

/** Use map to print URLs of web pages that contain the string {{{Scala}}}.
 *
 * @see https://github.com/jboner/akka/blob/releasing-2.0-M2/akka-docs/java/code/akka/docs/future/FutureDocTestBase.java */
class MapNonBlocking {
    /** executorService creates regular threads, which continue running when the application tries to exit. */
    private final ExecutorService executorService = Executors.newFixedThreadPool(10);

    /** Akka uses the execution context to manage futures under its control. This ExecutionContext creates regular threads. */
    private final ExecutionContext context = new ExecutionContext() {
        @Override public void execute(final Runnable runnable) { executorService.execute(runnable); }

        public void reportFailure(Throwable throwable) { System.err.println(throwable); }
    };

    /** Collection of futures, which Futures.sequence will turn into a Future of a collection.
     * Remember that HttpGetter implements Callable */
	private final ArrayList<Future<String>> futures = new ArrayList<Future<String>> (Arrays.asList(
        Futures.future(new HttpGetter("http://akka.io/"), context),
        Futures.future(new HttpGetter("http://www.playframework.org/"), context),
        Futures.future(new HttpGetter("http://nbronson.github.com/scala-stm/"), context)
    ));

    /** Accumulates result during fold(), also provides initial results, if desired. */
    protected final ArrayList<String> result = new ArrayList<String>();

    /** Composable function for both versions */
    private final Function2<ArrayList<String>, String, ArrayList<String>> applyFunction = new Function2<ArrayList<String>, String, ArrayList<String>>() {
        @Override public ArrayList<String> apply(final ArrayList<String> result, final String contents) {
            if (contents.contains("Scala"))
                result.add(contents);
            return result;
        }
    };

    private final OnComplete<ArrayList<String>> completionFn = new OnComplete<ArrayList<String>>() {

    	/** This method is executed asynchronously, probably after the mainline has completed */
        @Override public void onComplete(final Throwable exception, final ArrayList<String> result) {
            if (result != null) {
                System.out.println("Nonblocking Java map: " + result.size() + " web pages contained 'Scala'.");
            } else {
                System.out.println("Exception: " + exception);
            }
        }
    };

    private final OnComplete<ArrayList<String>> shutdownFn = new OnComplete<ArrayList<String>>() {
        @Override public void onComplete(final Throwable exception, final ArrayList<String> result) {
            executorService.shutdown(); // releases threadpool resources so System.exit() can run
        }
    };


    public void doit() {
    	result.clear();
        final Future<ArrayList<String>> resultFuture = Futures.fold(result, futures, applyFunction, context);
        resultFuture.andThen(completionFn).andThen(shutdownFn);
    }

    public static void main(final String[] args) {
        new MapNonBlocking().doit();
    }
}
