package com.micronautics.akka.dispatch.futureJava;


import akka.actor.ActorSystem;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.MessageDispatcher;
import akka.dispatch.OnComplete;
import akka.dispatch.OnFailure;
import akka.dispatch.OnSuccess;
import com.typesafe.config.ConfigFactory;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;

class OnFailureJava {
    private final String configString = "akka { logConfigOnStart=off }";
    ActorSystem system = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString));
    MessageDispatcher dispatcher = system.dispatcher();

    private final OnSuccess<Integer> successFn = new OnSuccess<Integer>() {
        @Override public void onSuccess(final Integer result) {
            System.out.println("Java OnFailure.onSuccess() result: " + result);
        }
    };

    private final OnFailure failureFn = new OnFailure() {
		public void onFailure(final Throwable exception) {
            System.err.println("OnFailure exception: " + exception);
        }
    };

    private final OnComplete<Iterable<Integer>> shutdownFn = new OnComplete<Iterable<Integer>>() {
        @Override public void onComplete(final Throwable exception, final Iterable<Integer> result) {
            System.out.println("Shutting down");
            system.shutdown(); // releases threadpool resources so System.exit() can run
        }
    };

    private final Callable<Integer> callable1 = new Callable<Integer>() {
        @Override public Integer call() { return 2 / 0; }
    };

    private final Callable<Integer> callable2 = new Callable<Integer>() {
        @Override public Integer call() { return 6 / 2; }
    };

    final Future<Integer> f1 = Futures.future(callable1, dispatcher);
    final Future<Integer> f2 = Futures.future(callable2, dispatcher);

    public void doit() {
        f1.onFailure(failureFn);
        f2.onSuccess(successFn).onFailure(failureFn); // can be chained together; call order not specified
        List<Future<Integer>> futures = new LinkedList<Future<Integer>>(Arrays.asList(f1, f2));
        Futures.sequence(futures, dispatcher).onComplete(shutdownFn);
    }

    public static void main(final String[] args) {
        new OnFailureJava().doit();
    }
}