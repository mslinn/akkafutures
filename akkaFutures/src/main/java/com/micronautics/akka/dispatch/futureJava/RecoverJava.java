package com.micronautics.akka.dispatch.futureJava;

import akka.actor.ActorSystem;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.MessageDispatcher;
import akka.dispatch.OnComplete;
import akka.dispatch.OnFailure;
import akka.dispatch.OnSuccess;
import akka.dispatch.Recover;
import com.typesafe.config.ConfigFactory;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;

class RecoverJava {
    private final String configString = "akka { logConfigOnStart=off }";
    ActorSystem system = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString));
    MessageDispatcher dispatcher = system.dispatcher();

    private final OnFailure failureFn = new OnFailure() {
        public void onFailure(final Throwable throwable) {
            System.out.println("RecoverJava onFailure thread " + Thread.currentThread().getName());
            System.out.println("RecoverJava onFailure: " + throwable.getMessage());
        }
    };

    private final OnSuccess<Integer> successFn = new OnSuccess<Integer>() {
        @Override public void onSuccess(final Integer result) {
            System.out.println("RecoverJava onSuccess thread " + Thread.currentThread().getName());
            System.out.println("RecoverJava onSuccess; result=" + result);
        }
    };

    private final Recover<Integer> recoverFn = new Recover<Integer>() {
        @Override public Integer recover(final Throwable exception) {
            System.out.println("RecoverJava recover thread " + Thread.currentThread().getName());
            System.err.println("RecoverJava returning largest Integer");
            return Integer.MAX_VALUE;
        }
    };

    private final OnComplete<Iterable<Integer>> shutdownFn = new OnComplete<Iterable<Integer>>() {
        @Override public void onComplete(final Throwable exception, final Iterable<Integer> result) {
            System.out.println("Shutting down");
            system.shutdown(); // releases threadpool resources so System.exit() can run
        }
    };

    private final Callable<Integer> callable1 = new Callable<Integer>() {
        // divide by 1 to see what happens when callable1 does not throw
        @Override public Integer call() {
            System.out.println("RecoverJava original Future thread " + Thread.currentThread().getName());
            return 2 / 0;
        }
    };

    final Future<Integer> f1 = Futures.future(callable1, dispatcher);

    public void doit() {
        final Future<Integer> f2 = f1.onSuccess(successFn).onFailure(failureFn)
                .recover(recoverFn).onSuccess(successFn).onFailure(failureFn);

        List<Future<Integer>> futures = new LinkedList<Future<Integer>>(Arrays.asList(f1, f2));
        Futures.sequence(futures, dispatcher).onComplete(shutdownFn);
    }

    public static void main(final String[] args) {
        new RecoverJava().doit();
    }
}