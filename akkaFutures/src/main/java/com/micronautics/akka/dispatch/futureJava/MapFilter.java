package com.micronautics.akka.dispatch.futureJava;

import akka.dispatch.Await;
import akka.dispatch.ExecutionContext;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.japi.Function;
import akka.japi.Option;
import akka.util.Duration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class MapFilter {
    /** executorService creates regular threads, which continue running when the application tries to exit. */
    private final ExecutorService executorService = Executors.newFixedThreadPool(10);

    private final ExecutionContext context = new ExecutionContext() {
        @Override public void execute(final Runnable runnable) { executorService.execute(runnable); }

        public void reportFailure(Throwable throwable) { System.err.println(throwable); }
    };

    /** Collection of futures, which Futures.traverse will turn into a Future of a collection. */
    private final ArrayList<Future<Option<Integer>>> futures = new ArrayList<Future<Option<Integer>>> (Arrays.asList(
    	Futures.future(new ExpCalc(1, 2), context),
        Futures.future(new ExpCalc(11, 2), context),
        Futures.future(new ExpCalc(3, 5),  context)
    ));

    /** Acts like the Scala List[Option[Int]].flatten()
     * @return list of Option.some() values, ignoring the Option.none() values */
    private Iterable<Integer> flatten(Iterable<Option<Integer>> list) {
        LinkedList<Integer> result = new LinkedList<Integer>();
        for (Option<Integer> item : list)
            if (item instanceof Option.Some)
                result.add(item.get());
        return result;
    }

    /** Return Future(None) if completed future result is >= 10 */
    private final Function<Future<Option<Integer>>, Future<Option<Integer>>> filterFn =
              new Function<Future<Option<Integer>>, Future<Option<Integer>>>() {
        public Future<Option<Integer>> apply(final Future<Option<Integer>> future) {
            Option<Integer> v = Option.none();
            if (!v.isEmpty() && v.get()<10)
                v = future.value().get().right().get();
            return Futures.future(new Value(v), context);
        }
    };

    /** Pretend this is an expensive computation */
    private class ExpCalc implements Callable<Option<Integer>> {
        private int a, b;

        public ExpCalc(int a, int b) {
            this.a = a;
            this.b = b;
        }

        @Override public Option<Integer> call() throws Exception {
            Thread.sleep(100);
            return Option.some(a + b);
        }
    }

    public void doit() {
    final Future<Iterable<Option<Integer>>> f = Futures.traverse(futures, filterFn, context);
    try {
        Iterable<Option<Integer>> result = Await.result(f, Duration.Inf());
        Iterable<Integer> resultFlattened = flatten(result);
        System.out.println("Java MapFilter result = " + resultFlattened);
    } catch (Exception ex) {
        System.err.println(ex);
    }
        executorService.shutdown();
    }

    public static void main(final String[] args) {
        new MapFilter().doit();
    }


    private class Value implements Callable<Option<Integer>> {
        private Option<Integer> value;

        public Value(Option<Integer> value) { this.value = value; }

        public Option<Integer> call() { return value; }
    }
}
