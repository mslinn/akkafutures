package com.micronautics.akka.dispatch.futureJava;

import akka.actor.ActorSystem;
import akka.dispatch.*;
import com.typesafe.config.ConfigFactory;

import java.util.concurrent.Callable;

/** Java version of:
 * <code>
 *  val a = 1
 *  val b = 2
 *  val c = 3
 *  val d = 4
 *  val e = 5
 *  val f = 6
 *  val future4 =
 *    Future(a+b).flatMap { x =>
 *      Future(c+d).flatMap { y =>
 *        Future(e+f).map { z => x + y + z }
 *      }
 *    }
 *  }
 *  future4 onComplete {
 *    case Right(r) => println(r)
 *    case Left(ex) => println(ex.getMessage)
 *  }
 * </code>
 */
class ForComprehension {
    private final String configString = "akka { logConfigOnStart=off }";
    ActorSystem system = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString));
    MessageDispatcher dispatcher = system.dispatcher();

    Future<Integer> future = Futures.future(new ExpCalc(1, 2), dispatcher)
        .flatMap(new Mapper<Integer, Future<Integer>>() {
            public Future<Integer> apply(final Integer x) {
                return Futures.future(new ExpCalc(3, 4), dispatcher)
                        .flatMap(new Mapper<Integer, Future<Integer>>() {
                            public Future<Integer> apply(final Integer y) {
                                return Futures.future(new ExpCalc(5, 6), dispatcher)
                                    .map(new Mapper<Integer, Integer>() {
                                        public Integer apply(final Integer z) {
                                            return x + y + z;
                                        }
                                    }
                                );
                            }
                        }
                    );
                }
            }
        );

    private final OnComplete<Integer> completionFn = new OnComplete<Integer>() {
        @Override public void onComplete(final Throwable throwable, final Integer result) {
            System.out.println("Java ForComprehension result = " + result);
        }
    };

    private final OnComplete<Integer> shutdownFn = new OnComplete<Integer>() {
        @Override public void onComplete(final Throwable throwable, final Integer result) {
            System.out.println("Shutting down");
            system.shutdown(); // allows program to System.exit()
        }
    };

    public void doit() {
        future.andThen(completionFn).andThen(shutdownFn);
    }

    public static void main(final String[] args) {
        new ForComprehension().doit();
    }


    /** Pretend this is an expensive computation */
    private class ExpCalc implements Callable<Integer> {
        private int a, b;

        public ExpCalc(int a, int b) {
            this.a = a;
            this.b = b;
        }

        @Override public Integer call() throws Exception { 
            return a + b;
        }
    }
}
