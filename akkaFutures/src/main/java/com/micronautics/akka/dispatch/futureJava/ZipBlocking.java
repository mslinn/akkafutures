package com.micronautics.akka.dispatch.futureJava;

import java.util.concurrent.Callable;
import scala.Tuple2;
import akka.actor.ActorSystem;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.MessageDispatcher;
import akka.dispatch.OnComplete;
import akka.dispatch.Recover;
import com.micronautics.concurrent.ExecutorUtils;
import com.typesafe.config.ConfigFactory;

class ZipBlocking {
    private final String configString = "akka { logConfigOnStart=off }";
    ActorSystem system = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString));
    MessageDispatcher dispatcher = system.dispatcher();


    /** No error handling makes things simpler */
    public Future<Tuple2<Integer, Integer>> doit1() {
        final Future<Integer> f1 = Futures.future(new Value(6, 3, 100), dispatcher);
        final Future<Integer> f2 = Futures.future(new Value(4, 2, 200), dispatcher);
       	return f1.zip(f2);
    }

    /** Error handling is better */
    public Future<Tuple2<Integer, Integer>> doit2() {
    	final Future<Integer> futureInt1 = Futures.future(new Value(6, 3, 100), dispatcher);
        final Future<Integer> futureException = Futures.future(new Value(6, 0, 200), dispatcher);
       	final Future<Tuple2<Integer, Integer>> futureZip2 = futureInt1.zip(futureException);
        futureZip2.recover(new Recover<Tuple2<Integer, Integer>>() {
            @Override
			public Tuple2<Integer, Integer> recover(Throwable throwable) throws Throwable {
                if (throwable instanceof ArithmeticException)
                    return new Tuple2<Integer, Integer>(0, 0);
                else
                    throw throwable;
            }
        });
        return futureZip2;
    }

    public void doit() {
        Future<Tuple2<Integer, Integer>> f1 = new ZipBlocking().doit1().onComplete(new OnComplete<Tuple2<Integer, Integer>>() {
            @Override
			public void onComplete(final Throwable throwable, final Tuple2<Integer, Integer> result) {
                if (result != null) {
                    System.out.println("Blocking Java Zip result 1: " + result);
                } else {
                    System.err.println("Blocking Java Zip exception 1: " + throwable.getMessage());
                }
            }
        });

        Future<Tuple2<Integer, Integer>> f2 = new ZipBlocking().doit2().onComplete(new OnComplete<Tuple2<Integer, Integer>>() {
            @Override
			public void onComplete(final Throwable throwable, final Tuple2<Integer, Integer> result) {
                if (result != null) {
                    System.out.println("Blocking Java Zip result 2: " + result);
                } else {
                    System.err.println("Blocking Java Zip exception 2: " + throwable.getMessage());
                }
            }
        });

        ExecutorUtils.exitWhenAllDone(dispatcher, f1, f2);
    }

    public static void main(final String[] args) {
        new ZipBlocking().doit();
    }

class Value implements Callable<Integer> {
    private Integer value1;
    private Integer value2;
    private long millis;

    public Value(Integer value1, Integer value2, long millis) {
        this.value1 = value1;
        this.value2 = value2;
        this.millis = millis;
    }

    @Override public Integer call() {
        try { Thread.sleep(millis); } catch (Exception ignored) {}
        return value1 / value2;
    }
}
}