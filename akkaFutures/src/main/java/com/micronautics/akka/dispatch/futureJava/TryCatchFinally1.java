package com.micronautics.akka.dispatch.futureJava;

import akka.actor.ActorSystem;
import akka.dispatch.Await;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.Mapper;
import akka.dispatch.MessageDispatcher;
import akka.dispatch.Recover;
import akka.util.Duration;
import com.typesafe.config.ConfigFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Callable;

class TryCatchFinally1 {
    private final String configString = "akka { logConfigOnStart=off }";
    private final ActorSystem system = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString));
    private final MessageDispatcher dispatcher = system.dispatcher();

    private final ArrayList<Future<Integer>> futures = new ArrayList<Future<Integer>>(Arrays.asList(
            Futures.future(new Kallable(1), dispatcher),
            Futures.future(new Kallable(2), dispatcher),
            Futures.future(new Kallable(3), dispatcher)
    ));

    private final Mapper<Integer, Integer> tryFn = new Mapper<Integer, Integer>() {
        public Integer apply(final Integer i) { return i / (i-1); }
    };

    private final Recover<Integer> recoverFn = new Recover<Integer>() {
        @Override public Integer recover(final Throwable throwable) {
            if (throwable instanceof ArithmeticException)
               return Integer.MAX_VALUE;
            else
                return 0;
        }
    };

    private final Mapper<Integer, Boolean> finallyFn = new Mapper<Integer, Boolean>() {
        public Boolean apply(final Integer i) { return i%2==0; }
    };

    public void doit() {
        for (Future<Integer> f : futures)
            try { // Calling Await.result() on a Future containing an Exception will cause the Exception to be rethrown
                System.out.println(Await.result(f.map(tryFn).recover(recoverFn).map(finallyFn), Duration.Inf()));
            } catch(Exception ignored) {}
        system.shutdown();
    }

    public static void main(final String[] args) {
        new TryCatchFinally1().doit();
    }

    private class Kallable implements Callable<Integer> {
        private Integer i;

        public Kallable(Integer i) { this.i = i; }

        @Override public Integer call() { return i; }
    }
}