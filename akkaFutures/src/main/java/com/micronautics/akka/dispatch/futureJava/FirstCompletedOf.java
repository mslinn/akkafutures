package com.micronautics.akka.dispatch.futureJava;

import akka.actor.ActorSystem;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.MessageDispatcher;
import akka.dispatch.OnComplete;
import com.typesafe.config.ConfigFactory;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;

class FirstCompletedOf {
    private final String configString = "akka { logConfigOnStart=off }";
    ActorSystem system = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString));
    MessageDispatcher dispatcher = system.dispatcher();

    private List<Future<String>> futures = new LinkedList<Future<String>> (Arrays.asList(
            Futures.future(new Value("asdf", 100), dispatcher),
            Futures.future(new Value("qwer", 200), dispatcher)));

    private final OnComplete<String> completionFn = new OnComplete<String>() {
		public void onComplete(final Throwable exception, final String result) {
            if (result != null) {
                System.out.println("FirstCompletedOf Java result: " + result);
            } else {
                System.out.println("FirstCompletedOf Java exception: " + exception.getMessage());
            }
        }
    };

    private final OnComplete<String> shutdownFn = new OnComplete<String>() {
        @Override public void onComplete(final Throwable exception, final String result) {
            system.shutdown(); // releases threadpool resources so System.exit() can run
        }
    };

    class Value implements Callable<String> {
        private String value;
        private long millis;

        public Value(String value, long millis) {
            this.value = value;
            this.millis = millis;
        }

        @Override public String call() {
            try { Thread.sleep(millis); } catch (Exception ignored) {}
            return value;
        }
    }


    public void doit() {
        final Future<String> fco = Futures.firstCompletedOf(futures, dispatcher);
        fco.andThen(completionFn).andThen(shutdownFn);
    }

    public static void main(final String[] args) {
        new FirstCompletedOf().doit();
    }
}