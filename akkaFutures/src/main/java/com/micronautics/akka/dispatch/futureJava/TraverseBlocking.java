package com.micronautics.akka.dispatch.futureJava;

import akka.actor.ActorSystem;
import akka.dispatch.Await;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.MessageDispatcher;
import akka.japi.Function;
import akka.util.Duration;
import com.micronautics.util.HttpGetter;
import com.typesafe.config.ConfigFactory;

import java.util.ArrayList;

import static java.util.concurrent.TimeUnit.SECONDS;

/** Invoke Future as a non-blocking function call, executed on another thread.
 *  This example uses traverse() to print URLs of web pages that contain the string {{{Scala}}}.
 * If this Future is completed with an exception then the new Future will also contain this exception.
 *
 * The Callable is not in scope of applyFunction2.apply(), unlike some other composable functions.
 * That means that public properties from cannot be retrieved from the Callable.
 * If this is important, HttpGetter.call() should be modified to return a result object, perhaps a HashMap, that wraps the url and the resulting content.
 * @see https://github.com/jboner/akka/blob/releasing-2.0-M2/akka-docs/java/code/akka/docs/future/FutureDocTestBase.java */
class TraverseBlocking {
    private final String configString = "akka { daemonic = on }";
    ActorSystem system = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString));
    MessageDispatcher dispatcher = system.dispatcher();

    /** Maximum length of time to block while waiting for futures to complete */
    private final Duration timeout = Duration.create(10, SECONDS);

    /** Collection of String, which Futures.traverse will turn into a Future of a collection.
     * The futures will run under daemonContext. */
    private final ArrayList<String> urls = new ArrayList<String>();

    /** Accumulates result during fold(), also provides initial results, if desired. */
    protected final ArrayList<String> initialValue = new ArrayList<String>();

    /** Composable function for both versions. Remember that HttpGetter implements Callable. */
    private final Function<String, Future<String>> applyFunction = new Function<String, Future<String>>() {
        @Override
		public Future<String> apply(final String url) {
        	// HttpGetter returns empty string if the fetched page does not contain the search string
            return Futures.future(new HttpGetter(url, "Scala"), dispatcher);
        }
    };


    {   /* Build array of URL Strings. */
    	urls.add("http://akka.io/");
        urls.add("http://www.playframework.org/");
        urls.add("http://nbronson.github.com/scala-stm/");
    }


    public void doit() {
    	final Future<Iterable<String>> resultFuture = Futures.traverse(urls, applyFunction, dispatcher);
        try {
            final Iterable<String> result = Await.result(resultFuture, timeout);
            int matchCount = 0;
            for (String r : result)
                if (r.length()>0)
                    matchCount++;
            System.out.println("Blocking Java traverse: " + matchCount + " web pages contained 'Scala'.");
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }

    /** Demonstrates how to invoke sequence() and block until a result is available */
    public static void main(final String[] args) {
    	new TraverseBlocking().doit();
    }
}