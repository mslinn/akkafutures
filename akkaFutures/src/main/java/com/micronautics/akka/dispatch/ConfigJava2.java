package com.micronautics.akka.dispatch;

import akka.actor.ActorSystem;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.MessageDispatcher;
import akka.dispatch.OnComplete;
import com.typesafe.config.ConfigFactory;

import java.util.concurrent.Callable;

/** Configure Akka system from a string */
class ConfigJava2 {
    private final String configString =
      "akka {\n" +
      "  logConfigOnStart=on # dumps out configuration onto console when enabled\n" +
      "  stdout-loglevel = \"WARNING\" # startup log level\n" +
      "  loglevel = \"INFO\"           # loglevel once ActorSystem is started\n" +
      "  actor {\n" +
      "    default-dispatcher {\n" +
      "      type = Dispatcher\n" +
      "      max-pool-size-max = 64  # default\n" +
      "      throughput = 5          # default\n" +
      "      core-pool-size-max = 64 # default\n" +
      "    }\n" +
      "  }\n" +
      "}\n";
    ActorSystem system = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString));
    MessageDispatcher dispatcher = system.dispatcher();

    private final OnComplete<Integer> completionFn = new OnComplete<Integer>() {
        @Override
		public void onComplete(final Throwable exception, final Integer result) {
            if (result != null) {
                System.out.println("ConfigJava2 result: " + result);
            } else {
                System.out.println("ConfigJava2 exception: " + exception);
            }
        }
    };

    private final OnComplete<Integer> shutdownFn = new OnComplete<Integer>() {
        @Override public void onComplete(final Throwable exception, final Integer result) {
            system.log().error("This an an error log message");
            system.shutdown(); // releases threadpool resources so System.exit() can run
        }
    };

    private final Callable<Integer> callable = new Callable<Integer>() {
        @Override public Integer call() { return 2 + 3; }
    };


    public void doit() {
        final Future<Integer> resultFuture = Futures.future(callable, dispatcher);
        resultFuture.andThen(completionFn).andThen(shutdownFn);
    }

    public static void main(final String[] args) {
        new ConfigJava2().doit();
    }
}