package com.micronautics.akka.dispatch.futureJava;

import akka.actor.ActorSystem;
import akka.dispatch.Await;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.MessageDispatcher;
import akka.util.Duration;
import com.typesafe.config.ConfigFactory;

import java.util.ArrayList;

import static akka.japi.Util.manifest;

class SequenceJava {
    private final String configString = "akka { logConfigOnStart=off }";
    ActorSystem system = ActorSystem.apply("actorSystem", ConfigFactory.parseString(configString));
    MessageDispatcher dispatcher = system.dispatcher();

    /** Collection of futures, which Futures.sequence will turn into a Future of a collection. */
    private final ArrayList<Future<Integer>> futures = new ArrayList<Future<Integer>>();

    public void doit() {
        for (Integer i=1; i<10; i++) {
            // create a Future with a preset value (i*2)
            Future<Integer> f = Futures.promise(dispatcher).success(i*2).future().mapTo(manifest(Integer.class));
            futures.add(f);
        }
        final Future<Iterable<Integer>> future = Futures.sequence(futures, dispatcher);
        try {// Await.result() blocks until the Future completes
            final Iterable<Integer> result = Await.result(future, Duration.Inf());
            System.out.println("BLocking Java sequence: " + result);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        } finally {
            system.shutdown();
        }
    }

    public static void main(final String[] args) {
    	new SequenceJava().doit();
    }
}