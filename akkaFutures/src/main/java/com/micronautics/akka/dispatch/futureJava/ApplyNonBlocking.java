package com.micronautics.akka.dispatch.futureJava;

import akka.dispatch.ExecutionContext;
import akka.dispatch.Future;
import akka.dispatch.Futures;
import akka.dispatch.OnComplete;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/** This example uses Future.future() to create a future that computes 2+3. */
class ApplyNonBlocking {
    /** executorService creates regular threads, which continue running when the application tries to exit. */
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    /** Akka uses the execution context to manage futures under its control. This ExecutionContext creates regular threads. */
    private final ExecutionContext context = new ExecutionContext() {
        @Override public void execute(final Runnable runnable) { executorService.execute(runnable); }

        public void reportFailure(Throwable throwable) { System.err.println(throwable); }
    };

    private final OnComplete<Integer> completionFn = new OnComplete<Integer>() {
        @Override
		public void onComplete(final Throwable exception, final Integer result) {
            if (result != null) {
                System.out.println("Nonblocking Java apply result: " + result);
            } else {
                System.out.println("Nonblocking Java apply exception: " + exception);
            }
        }
    };

    private final OnComplete<Integer> shutdownFn = new OnComplete<Integer>() {
        @Override public void onComplete(final Throwable exception, final Integer result) {
            executorService.shutdown(); // releases threadpool resources so System.exit() can run
        }
    };

    private final Callable<Integer> callable = new Callable<Integer>() {
        @Override public Integer call() { return 2 + 3; }
    };


    public void doit() {
        final Future<Integer> f = Futures.future(callable, context);
        f.andThen(completionFn).andThen(shutdownFn);
    }

    public static void main(final String[] args) {
        new ApplyNonBlocking().doit();
    }
}