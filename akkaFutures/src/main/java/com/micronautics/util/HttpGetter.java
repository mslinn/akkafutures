package com.micronautics.util;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import java.util.concurrent.Callable;


/** Called when a future is about to be computed.
 * @author Mike Slinn*/
public class HttpGetter implements Callable<String> {
    private DefaultHttpClient httpClient = new DefaultHttpClient();
    private BasicResponseHandler brh = new BasicResponseHandler();
    private String urlStr;
    private String searchString = null;


    /** @param urlStr web page URL to fetch */
    public HttpGetter(final String urlStr) { this.urlStr = urlStr; }

    /** @param urlStr web page URL to fetch
     * @param searchString web page must contain searchString in order to be returned */
    public HttpGetter(final String urlStr, final String searchString) {
    	this.urlStr = urlStr;
    	this.searchString = searchString;
    }

    /** @return web page contents if search string not specified or search string found in contents;
     * otherwise return empty string */
    @Override public String call() throws Exception {
        System.out.println("HttpGetter call() thread (runs on Future's thread) " + Thread.currentThread().getName());
        final HttpGet httpGet = new HttpGet(urlStr);
        final String result = httpClient.execute(httpGet, brh);
        if (searchString==null || result.indexOf(searchString)>=0)
            return result;
        return "";
    }
}
