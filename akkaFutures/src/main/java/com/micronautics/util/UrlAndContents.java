package com.micronautics.util;

public class UrlAndContents {
	public String url;
	public String contents;
	
	public UrlAndContents(String url, String contents) {
		this.url = url;
		this.contents = contents;
	}
}
