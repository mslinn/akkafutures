package com.micronautics.scala;

import akka.japi.Option;
import scala.runtime.AbstractFunction0;

public class OptionDemo {
    final Option<String> object1 = Option.option("Hi there");
    final Option<String> osName = Option.option(System.getProperty("os.name"));
    final AbstractFunction0<String> orElseFn = new AbstractFunction0<String>() {
    	public String apply() { return "default"; }
	};

    final Option<String> notPresentOption = Option.option(System.getProperty("not.present"));
    final String notPresent = Option.java2ScalaOption(notPresentOption).getOrElse(orElseFn);

    public static void main(String[] args) {
    	OptionDemo od = new OptionDemo();
    	System.out.println("object1 value:    " + od.object1.get());
    	System.out.println("osName value:     " + od.osName.get());
    	System.out.println("notPresent value: " + od.notPresent);
    }
}
