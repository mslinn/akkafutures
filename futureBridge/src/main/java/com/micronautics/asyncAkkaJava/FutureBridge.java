package com.micronautics.asyncAkkaJava;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import scala.Either;
import scala.Left;
import scala.Right;

import akka.dispatch.Await;
import akka.dispatch.ExecutionContext;
import akka.dispatch.Futures;
import akka.dispatch.Future;
import akka.dispatch.Promise;
import akka.util.Duration;

import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.ListenableFuture;
import com.ning.http.client.Response;

public class FutureBridge {
	private final AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
	private final ExecutorService executorService = asyncHttpClient.getConfig().executorService();
	private final ExecutionContext context = new ExecutionContext() {
        @Override
		public void execute(Runnable runnable) { executorService.execute(runnable); }
    };

	private Future<Response> bridge(final ListenableFuture<Response> ahcf, ExecutionContext context, ExecutorService executorService) {
	    final Promise<Response> promise = Futures.promise(context);
		ahcf.addListener(
			new Runnable() {
				@Override public void run() {
					Either<Throwable, Response> either;
				    try {
                        either = new Right<Throwable, Response>(ahcf.get(0, TimeUnit.MILLISECONDS));
				    	promise.complete(either);
				    } catch (Exception e) {
                        either = new Left<Throwable, Response>(e);
				    }
			    	promise.complete(either);
				}
		    }, executorService);
	    return promise;
	}

	private void doit() throws IOException {
		final ListenableFuture<Response> listenableFuture = asyncHttpClient.prepareGet("http://www.ning.com").execute();
		Future<Response> future = bridge(listenableFuture, context, executorService);
		Response response = Await.result(future, Duration.create(1, SECONDS));
		System.out.println("FutureBridge Java result: " + response.getResponseBody());
		asyncHttpClient.close(); // Close the AsyncHttpClient connections
	}

	public static void main(String[] args) throws IOException {
		new FutureBridge().doit();
	}
}
