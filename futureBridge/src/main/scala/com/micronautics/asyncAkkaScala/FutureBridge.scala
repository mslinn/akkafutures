package com.micronautics.asyncAkkaScala

import java.util.concurrent.TimeUnit

import com.ning.http.client.ListenableFuture
import com.ning.http.client.AsyncHttpClient

import akka.dispatch.ExecutionContext
import akka.dispatch.Future
import akka.dispatch.Promise

object FutureBridge extends App {
  val asyncHttpClient  = new AsyncHttpClient()
  val executorService = asyncHttpClient.getConfig().executorService()
  implicit val context = ExecutionContext.fromExecutor(executorService)
  val listenableFuture = asyncHttpClient.prepareGet("http://www.ning.com").execute()
  bridge(listenableFuture) onComplete { f =>
    f match {
      case Right(result)   => println("FutureBridge Scala result: " + result.getResponseBody())
      case Left(exception) => println("FutureBridge Scala exception: " + exception)
    }
    asyncHttpClient.close() // Close the AsyncHttpClient connections
  }
  
  
  def bridge[T](listenableFuture: ListenableFuture[T]): Future[T] = {
	val promise = Promise[T]()
	listenableFuture.addListener(new Runnable { 
	  def run: Unit = promise.complete(
	    try Right(listenableFuture.get(1, TimeUnit.SECONDS)) 
	    catch { case e => Left(e) }
	  )
    }, executorService)
    promise
  }
}