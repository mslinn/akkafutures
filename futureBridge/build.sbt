// see https://github.com/sbt/sbt-assembly
import AssemblyKeys._ // put this at the top of the file

name := "futureBridge"

version := "0.1"

scalaVersion := "2.9.1"

scalacOptions ++= Seq("-deprecation")

resolvers ++= Seq(
  "Typesafe Snapshots"    at "http://repo.typesafe.com/typesafe/snapshots",
  "Typesafe Releases"     at "http://repo.typesafe.com/typesafe/releases",
  "Scala-Tools Snapshots" at "http://scala-tools.org/repo-snapshots",
  "Scala Tools Releases"  at "http://scala-tools.org/repo-releases"
)

libraryDependencies ++= Seq(
  "org.scalatest"     %% "scalatest"              % "latest.integration"  % "test"    withSources(),
  "com.typesafe.akka" %  "akka-actor"             % "latest.milestone"    withSources(),
  "com.ning"          % "async-http-client"       % "1.6.4"               withSources(),
//"ch.qos.logback"    % "logback-classic"         % "0.9.29"              withSources(),
  "org.slf4j"         %  "slf4j-nop"              % "1.6.0"               withSources(),
  "org.scala-tools"   %% "scala-stm"              % "0.5-SNAPSHOT"        withSources()
)

seq(assemblySettings: _*)

